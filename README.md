# Gnomon plugin package : `Tissue Mesh`

This package contains Python plugins for the [Gnomon computational platform](https://gnomon.gitlabpages.inria.fr/gnomon/)
to manipulate 3D topological representations (Triangular meshes and Cellular complexes).

This package contains plugins implementing the gnomon forms **mesh** and **cellComplex**, visualization for those forms
and various algorithms to manipulate them.

## Installation

This package is published on the [gnomon anaconda channel](https://anaconda.org/gnomon/gnomon_package_tissuemesh).
To install it a **conda client** is needed such as [miniconda](https://docs.anaconda.com/miniconda/).

To install use the following command in your **gnomon** environment:

```shell
gnomon-utils package install gnomon_package_tissuemesh
```

Alternatively use:
```shell
conda install -c conda-forge -c gnomon -c mosaic -c morpheme -c dtk-forge6 gnomon_package_tissuemesh
```