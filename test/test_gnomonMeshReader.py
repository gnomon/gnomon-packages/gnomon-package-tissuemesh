import unittest

import gnomon.core
from gnomon.utils import load_plugin_group


class TestGnomonMeshReader(unittest.TestCase):
    """
    Tests the gnomonMeshReader class.
    """

    @classmethod
    def setUpClass(cls):
        load_plugin_group("meshReader")

    def setUp(self):
        self.filename = "test/resources/hexagons.ply"

        self.reader = gnomon.core.meshReader_pluginFactory().create("gnomonMeshReaderPropertyTopomesh")
        self.reader.setPath(self.filename)

    def tearDown(self):
        self.reader.this.disown()

    def test_gnomonMeshReader_read(self):
        self.reader.run()
        mesh = self.reader.mesh()[0]
        assert mesh is not None

    def test_gnomonMeshReader_mesh(self):
        self.reader.run()
        mesh = self.reader.mesh()[0]
        assert mesh.cellsCount(2) == 42
        assert mesh.cellsCount(0) == 31

    def test_gnomonMeshReader_type(self):
        self.reader.run()
        type = self.reader.extensions()
        assert type == ['ply']

#
# test_gnomonMeshReader.py ends here.
