import unittest

import gnomon.core
from gnomon.utils import load_plugin_group


class TestGnomonCellComplexReader(unittest.TestCase):
    """Tests the gnomonCellComplexReader class.

    """

    @classmethod
    def setUpClass(cls):
        load_plugin_group("cellComplexReader")

    def setUp(self):
        self.filename = "test/resources/hexagons.ply"

        self.reader = gnomon.core.cellComplexReader_pluginFactory().create("gnomonCellComplexReaderPropertyTopomesh")
        self.reader.setPath(self.filename)

    def tearDown(self):
        self.reader.this.disown()

    def test_gnomonCellComplexReader_read(self):
        self.reader.run()
        cellComplex = self.reader.cellComplex()[0]
        assert cellComplex is not None

    def test_gnomonCellComplexReader_cellComplex(self):
        self.reader.run()
        cellComplex = self.reader.cellComplex()[0]
        assert cellComplex.elementCount(3) == 7

#
# test_gnomonCellComplexReader.py ends here.
