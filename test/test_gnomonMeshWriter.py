import unittest
import os

import gnomon.core
from gnomon.utils import load_plugin_group



class TestGnomonMeshWriter(unittest.TestCase):
    """Tests the gnomonMeshWriter class.

    """

    @classmethod
    def setUpClass(cls):
        load_plugin_group("meshReader")
        load_plugin_group("meshWriter")

    def setUp(self):
        self.filename = "test/resources/hexagons.ply"

        self.reader = gnomon.core.meshReader_pluginFactory().create("gnomonMeshReaderPropertyTopomesh")
        self.reader.setPath(self.filename)

        self.reader.run()
        self.mesh = self.reader.mesh()

        self.saved_filename = "test/resources/mesh_writer_tmp.ply"

    def tearDown(self):
        os.remove(self.saved_filename)
        self.reader.this.disown()
        self.writer.this.disown()

    def test_gnomonMeshWriter_write(self):

        self.writer = gnomon.core.meshWriter_pluginFactory().create("gnomonMeshWriterPropertyTopomesh")
        self.writer.setPath(self.saved_filename)
        self.writer.setMesh(self.mesh)
        self.writer.run()
        assert os.path.exists(self.saved_filename)

        self.reader.setPath(self.saved_filename)
        self.reader.run()
        read_mesh = self.reader.mesh()
        assert read_mesh is not None

        # all_voxels = np.prod(dtk_img_to_sp_img(read_mesh.mesh()).shape)
        # ok_voxels = np.isclose(dtk_img_to_sp_img(self.mesh.mesh()).get_array(),dtk_img_to_sp_img(read_mesh.mesh()).get_array(),10).sum()
        # assert ok_voxels/float(all_voxels)>0.95



#
# test_gnomonMeshWriter.py ends here.
