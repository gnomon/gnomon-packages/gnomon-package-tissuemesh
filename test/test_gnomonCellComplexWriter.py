import unittest
import os

import gnomon.core
from gnomon.utils import load_plugin_group


class TestGnomonCellComplexWriter(unittest.TestCase):
    """Tests the gnomonCellComplexWriter class.

    """

    @classmethod
    def setUpClass(cls):
        load_plugin_group("cellComplexReader")
        load_plugin_group("cellComplexWriter")

    def setUp(self):
        self.filename = "test/resources/hexagons.ply"

        self.reader = gnomon.core.cellComplexReader_pluginFactory().create("gnomonCellComplexReaderPropertyTopomesh")
        self.reader.setPath(self.filename)

        self.reader.run()
        self.cellcomplex = self.reader.cellComplex()

        self.saved_filename = "test/resources/cellcomplex_writer_tmp.ply"

        self.writer = gnomon.core.cellComplexWriter_pluginFactory().create("gnomonCellComplexWriterPropertyTopomesh")
        self.writer.setPath(self.saved_filename)

    def tearDown(self):
        if os.path.exists(self.saved_filename):
            os.remove(self.saved_filename)
        self.reader.this.disown()
        self.writer.this.disown()

    def test_gnomonCellComplexWriter_write(self):
        self.writer.setCellComplex(self.cellcomplex)
        self.writer.run()
        assert os.path.exists(self.saved_filename)

        self.reader.setPath(self.saved_filename)
        self.reader.run()
        read_cellcomplex = self.reader.cellComplex()
        assert read_cellcomplex is not None

        # all_voxels = np.prod(dtk_img_to_sp_img(read_cellcomplex.cellcomplex()).shape)
        # ok_voxels = np.isclose(dtk_img_to_sp_img(self.cellcomplex.cellcomplex()).get_array(),dtk_img_to_sp_img(read_cellcomplex.cellcomplex()).get_array(),10).sum()
        # assert ok_voxels/float(all_voxels)>0.95



#
# test_gnomonCellComplexWriter.py ends here.
