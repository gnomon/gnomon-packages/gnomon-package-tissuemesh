# Version: $Id$
#
#

# Commentary:
#
#

# Change Log:
#
#

# Code:

import unittest

import numpy as np

import gnomon.core
from gnomon.utils import load_plugin_group


class TestGnomonMeshFilter(unittest.TestCase):
    """Tests the gnomonMeshFilter class.

    """

    @classmethod
    def setUpClass(cls):
        load_plugin_group("meshReader")
        load_plugin_group("meshFilter")

    def setUp(self):
        self.filename = "test/resources/hexagons.ply"

        self.reader = gnomon.core.meshReader_pluginFactory().create("gnomonMeshReaderPropertyTopomesh")
        self.reader.setPath(self.filename)
        self.reader.run()

        self.mesh = self.reader.mesh()

        self.filter = gnomon.core.meshFilter_pluginFactory().create("meshSmoothingCellcomplex")
        self.filter.setInput(self.mesh)
        self.filter.refreshParameters()

    def tearDown(self):
        self.reader.this.disown()
        self.filter.this.disown()

    def test_gnomonMeshFilter_parameters(self):
        self.filter.setParameter('iterations',5)
        assert self.filter['iterations'] == 5

    def test_gnomonMeshFilter_filter(self):
        self.filter.run()
        mesh = self.filter.output()[0]
        assert mesh.cellsCount(2) == self.mesh[0].cellsCount(2)

        mesh_x = self.mesh[0].attribute('barycenter_x')['data']
        filtered_x = mesh.attribute('barycenter_x')['data']
        assert np.any([m_x != f_x for m_x, f_x in zip(mesh_x, filtered_x)])

#
# test_gnomonMeshFilter.py ends here.
