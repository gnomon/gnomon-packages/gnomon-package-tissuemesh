import unittest

import gnomon.core
from gnomon.core import gnomonMesh

from cellcomplex.property_topomesh.example_topomesh import sphere_topomesh
from cellcomplex.property_topomesh.analysis import compute_topomesh_property
from cellcomplex.property_topomesh.analysis import compute_topomesh_vertex_property_from_faces

from gnomon.utils import load_plugin_group


class TestGnomonMesh(unittest.TestCase):
    """Test the gnomonMesh class.

    """

    @classmethod
    def setUpClass(cls):
        load_plugin_group("meshData")

    def setUp(self):
        # -- Creating a PropertyTopomesh to build up our gnomonMesh
        self.topomesh = sphere_topomesh()
        compute_topomesh_property(self.topomesh,'barycenter',2)
        compute_topomesh_property(self.topomesh,'area',2)
        compute_topomesh_property(self.topomesh,'normal',2,normal_method='orientation')
        compute_topomesh_vertex_property_from_faces(self.topomesh,'normal',neighborhood=3,adjacency_sigma=1.2)

        # -- Creating a gnomonMesh from a PropertyTopomesh
        self.mesh = gnomonMesh()
        self.mesh_data = gnomon.core.meshData_pluginFactory().create("gnomonMeshDataPropertyTopomesh")
        self.mesh_data.set_property_topomesh(self.topomesh)
        self.mesh.setData(self.mesh_data)

    def tearDown(self):
        self.mesh.this.disown()
        self.mesh_data.this.disown()

    def test_gnomonMesh_elements(self):
        assert self.mesh.cellsIdx(0) == list(self.topomesh.wisps(0))
        assert self.mesh.cellsCount(0) == self.topomesh.nb_wisps(0)

        assert self.mesh.cellsIdx(2) == list(self.topomesh.wisps(2))
        assert self.mesh.cellsCount(2) == self.topomesh.nb_wisps(2)

        for tid in self.mesh.cellsIdx(2):
            assert set(self.mesh.cellPointsIdx(tid)) == set(self.topomesh.borders(2, tid, 2))

    def test_gnomonMeshData_fromGnomonForm(self):
        new_mesh_data = gnomon.core.meshData_pluginFactory().create("gnomonMeshDataPropertyTopomesh")
        new_mesh_data.fromGnomonForm(self.mesh)

        assert new_mesh_data.cellsCount(0) == self.mesh.cellsCount(0)
        assert new_mesh_data.cellsCount(2) == self.mesh.cellsCount(2)

    def test_gnomonMesh_attributes(self):

        for property_name in self.mesh.attributesNames():
            assert self.topomesh.has_wisp_property(property_name, 0) or self.topomesh.has_wisp_property(property_name, 2)
            attr = self.mesh.attribute(property_name)


if __name__ == '__main__':
    load_plugin_group("meshData")

    # -- Creating a PropertyTopomesh to build up our gnomonMesh
    topomesh = sphere_topomesh()
    compute_topomesh_property(topomesh,'barycenter',2)
    compute_topomesh_property(topomesh,'area',2)
    compute_topomesh_property(topomesh,'normal',2,normal_method='orientation')
    compute_topomesh_vertex_property_from_faces(topomesh,'normal',neighborhood=3,adjacency_sigma=1.2)

    # -- Creating a gnomonMesh from a PropertyTopomesh
    mesh = gnomonMesh()
    mesh_data = gnomon.core.meshData_pluginFactory().create("gnomonMeshDataPropertyTopomesh")
    mesh_data.set_property_topomesh(topomesh)
    mesh.setData(mesh_data)

    assert mesh.cellsCount(0) == topomesh.nb_wisps(0)
    assert mesh.cellsCount(2) == topomesh.nb_wisps(2)

    for property_name in mesh.attributesNames():
        assert topomesh.has_wisp_property(property_name, 0) or topomesh.has_wisp_property(property_name, 2)
        attr = mesh.attribute(property_name)






#
# test_gnomonMesh.py ends here.
