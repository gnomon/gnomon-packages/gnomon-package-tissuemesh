#!/usr/bin/env python
# -*- coding: utf-8 -*-

# {# pkglts, pysetup.kwds
# format setup arguments

from setuptools import setup, find_packages

short_descr = "Gnomon python plugins to manipulate 3D topological representations"
readme = open('README.md').read()


# find packages
pkgs = find_packages('src')

setup_kwds = dict(
    name='gnomon_package_tissuemesh',
    version="1.0.0",
    description=short_descr,
    long_description=readme,
    author="gnomon-dev",
    author_email="amdt-gnomon-dev@inria.fr",
    url='',
    license='LGPL-3.0-or-later',
    zip_safe=False,

    packages=pkgs,
    
    package_dir={'': 'src'},
    entry_points={
        'cellComplexData': [
            'gnomonCellComplexDataPropertyTopomesh = gnomon_package_tissuemesh.form.cellComplexData.gnomonCellComplexDataPropertyTopomesh'
        ],
        'meshData': [
            'gnomonMeshDataPropertyTopomesh = gnomon_package_tissuemesh.form.meshData.gnomonMeshDataPropertyTopomesh'
        ],
        'cellComplexReader': [
            'gnomonCellComplexReaderPropertyTopomesh = gnomon_package_tissuemesh.io.cellComplexReader.gnomonCellComplexReaderPropertyTopomesh'
        ],
        'meshReader': [
            'gnomonMeshReaderPropertyTopomesh = gnomon_package_tissuemesh.io.meshReader.gnomonMeshReaderPropertyTopomesh'
        ],
        'cellComplexWriter': [
            'gnomonCellComplexWriterPropertyTopomesh = gnomon_package_tissuemesh.io.cellComplexWriter.gnomonCellComplexWriterPropertyTopomesh'
        ],
        'meshWriter': [
            'gnomonMeshWriterPropertyTopomesh = gnomon_package_tissuemesh.io.meshWriter.gnomonMeshWriterPropertyTopomesh'
        ],
        'cellComplexConstructor': [
            'hexagonalPrismCellComplex = gnomon_package_tissuemesh.algorithm.cellComplexConstructor.hexagonalPrismCellComplex',
            'circleVoronoiCellComplex = gnomon_package_tissuemesh.algorithm.cellComplexConstructor.circleVoronoiCellComplex'
        ],
        'meshConstructor': [
            'ellipsoidMeshConstructor = gnomon_package_tissuemesh.algorithm.meshConstructor.ellipsoidMeshConstructor'
        ],
        'cellComplexAdapter': [
            'cellComplexToMeshDelaunay = gnomon_package_tissuemesh.adapter.cellComplexAdapter.cellComplexToMeshDelaunay',
            'cellComplexToMeshStarTriangulation = gnomon_package_tissuemesh.adapter.cellComplexAdapter.cellComplexToMeshStarTriangulation'
        ],
        'meshAdapter': [
            'meshToPointCloudTriangleCenterTopomesh = gnomon_package_tissuemesh.adapter.meshAdapter.meshToPointCloudTriangleCenterTopomesh',
            'meshToPointCloudVertexTopomesh = gnomon_package_tissuemesh.adapter.meshAdapter.meshToPointCloudVertexTopomesh'
        ],
        'meshFilter': [
                'isotropicRemeshingCellcomplex = gnomon_package_tissuemesh.algorithm.meshFilter.isotropicRemeshingCellcomplex',
            'meshSmoothingCellcomplex = gnomon_package_tissuemesh.algorithm.meshFilter.meshSmoothingCellcomplex',
            'triangleMeshCurvature = gnomon_package_tissuemesh.algorithm.meshFilter.triangleMeshCurvature',
            'vertexPropertyWatershedSegmentation = gnomon_package_tissuemesh.algorithm.meshFilter.vertexPropertyWatershedSegmentation'
        ],
        'cellComplexVtkVisualization': [
            'gnomonCellComplexVtkVisualizationCellProperty = gnomon_package_tissuemesh.visualization.cellComplexVtkVisualization.gnomonCellComplexVtkVisualizationCellProperty'
        ],
        'meshVtkVisualization': [
            'meshVisualizationTopomesh = gnomon_package_tissuemesh.visualization.meshVtkVisualization.meshVisualizationTopomesh',
            'meshVisualizationPyvista = gnomon_package_tissuemesh.visualization.meshVtkVisualization.meshVisualizationPyvista'
        ]
    },
    keywords='',
    
    test_suite='nose.collector',
    )
# #}


# do not change things below
# {# pkglts, pysetup.call
setup(**setup_kwds)
# #}
