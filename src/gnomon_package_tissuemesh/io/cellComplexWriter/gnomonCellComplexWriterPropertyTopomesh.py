import os
import logging

import gnomon.core
from gnomon.core import gnomonAbstractCellComplexWriter

from cellcomplex.property_topomesh.io import save_ply_property_topomesh

from gnomon.utils import load_plugin_group, algorithmPlugin, seriesWriter
from gnomon.utils.decorators import cellComplexInput


@algorithmPlugin(version="1.0.0", coreversion="1.0.0")
@cellComplexInput('topomesh', data_plugin="gnomonCellComplexDataPropertyTopomesh")
@seriesWriter('topomesh', 'filepath')
class gnomonCellComplexWriterPropertyTopomesh(gnomonAbstractCellComplexWriter):
    def __init__(self):
        super().__init__()
        self.filepath = None
        self.topomesh = {}

    def setPath(self, filepath):
        self.filepath = filepath

    def run(self):
        ext = os.path.splitext(self.filepath)[1]

        if ext in [".ply"]:
            times = list(self.topomesh.keys())
            if len(times) > 1:
                for time in times:
                    topomesh = self.topomesh[time]

                    properties_to_save = {}
                    for degree in range(4):
                        # properties_to_save[degree] = [p for p in topomesh.wisp_property_names(degree) if not((degree==0) and ('barycenter' in p))]
                        properties_to_save[degree] = []

                    (dirname, filename) = os.path.split(self.filepath)
                    dirpath = dirname + "/" + os.path.splitext(filename)[0]
                    if not os.path.exists(dirpath):
                        os.makedirs(dirpath)

                    time_filepath = os.path.join(dirpath, os.path.splitext(filename)[0] + "_t" + "%05.2f" % (time) + ext)
                    save_ply_property_topomesh(topomesh, time_filepath)
            else:
                topomesh = self.topomesh[times[0]]

                properties_to_save = {}
                for degree in range(4):
                    # properties_to_save[degree] = [p for p in topomesh.wisp_property_names(degree) if not((degree==0) and ('barycenter' in p))]
                    properties_to_save[degree] = []
                save_ply_property_topomesh(topomesh, self.filepath, properties_to_save=properties_to_save)

    def extensions(self):
        return ["ply"]