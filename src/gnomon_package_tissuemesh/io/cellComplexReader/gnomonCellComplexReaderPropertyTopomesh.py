import os

import gnomon.core

from gnomon.core import gnomonAbstractCellComplexReader
from cellcomplex.property_topomesh.io import read_ply_property_topomesh
from cellcomplex.property_topomesh.analysis import compute_topomesh_property

from gnomon.utils import algorithmPlugin, seriesReader
from gnomon.utils.decorators import cellComplexOutput


@algorithmPlugin(version="1.0.0", coreversion="1.0.0")
@cellComplexOutput('topomesh', data_plugin="gnomonCellComplexDataPropertyTopomesh")
@seriesReader('topomesh', 'path')
class gnomonCellComplexReaderPropertyTopomesh(gnomonAbstractCellComplexReader):
    """Read a 3D cellular complex from a PLY file.

    The function loads a complex stored in a .ply file and builds a cellular
    complex data structure based on its elements. If the file defines volume
    elements, they constitute cells of the resulting complex. Otherwise, a
    single cell is created with its boundary defined by all the faces in the
    complex.
    """

    def __init__(self):
        super().__init__()

        self.path = None
        self.topomesh = {}

    def __del__(self):
        pass

    def run(self):
        self.topomesh = {}

        paths = self.path.split(",")

        for time, path in enumerate(paths):
            if os.path.exists(path) and any([path.endswith(e) for e in self.extensions()]):

                topomesh = read_ply_property_topomesh(path)

                for degree in [1, 2, 3]:
                    compute_topomesh_property(topomesh, 'barycenter', degree)

                for degree in [0, 1, 2, 3]:
                    positions = topomesh.wisp_property('barycenter', degree)
                    for k, dim in enumerate(['x', 'y', 'z']):
                        topomesh.update_wisp_property('barycenter_'+dim, degree, dict(zip(positions.keys(), positions.values()[:, k])))

                self.topomesh[time] = topomesh

    def setPath(self, path):
        self.path = path

    def extensions(self):
        return ["ply"]
