import os

import gnomon.core
from gnomon.core import gnomonAbstractMeshReader
from cellcomplex.property_topomesh.io import read_ply_property_topomesh
from cellcomplex.property_topomesh.analysis import compute_topomesh_property, is_triangular

from gnomon.utils import algorithmPlugin, seriesReader
from gnomon.utils.decorators import meshOutput

@algorithmPlugin(version="1.0.0", coreversion="1.0.0")
@meshOutput('topomesh', data_plugin="gnomonMeshDataPropertyTopomesh")
@seriesReader('topomesh', 'path')
class gnomonMeshReaderPropertyTopomesh(gnomonAbstractMeshReader):
    """Read a PLY file as a 3D triangular mesh.

    The function loads a mesh stored in a .ply file and, provided it only has
    triangular faces, stores in a triangular mesh data structure. If attributes
    are attached to faces, they are stored as properties on the triangles of
    the mesh.
    """

    def __init__(self):
        super().__init__()

        self.path = None
        self.topomesh = {}

    def __del__(self):
        pass

    def run(self):
        self.topomesh = {}

        paths = self.path.split(",")

        self.set_max_progress(2*len(paths))
        for time, path in enumerate(paths):
            if os.path.exists(path) and any([path.endswith(e) for e in self.extensions()]):
                self.set_progress_message(f"T {time} : Reading mesh file")
                topomesh = read_ply_property_topomesh(path)
                self.increment_progress()

                self.set_progress_message(f"T {time} : Updating mesh")
                assert is_triangular(topomesh)

                for degree in [1, 2, 3]:
                    compute_topomesh_property(topomesh, 'barycenter', degree)

                for degree in [0, 1, 2, 3]:
                    positions = topomesh.wisp_property('barycenter', degree)
                    for k, dim in enumerate(['x', 'y', 'z']):
                        topomesh.update_wisp_property('barycenter_'+dim, degree, dict(zip(positions.keys(), positions.values()[:, k])))
                self.increment_progress()

                self.topomesh[time] = topomesh
            else:
                self.increment_progress(2)

    def setPath(self, path):
        self.path = path

    def extensions(self):
        return ["ply"]
