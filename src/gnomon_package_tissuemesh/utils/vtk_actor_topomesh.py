# Version: $Id$
#
#

# Commentary:
#
#

# Change Log:
#
#

# Code:

import unittest
import sys

#from PyQt5.QtCore import QSettings

# settings = QSettings(QSettings.IniFormat, QSettings.UserScope, "inria", "gnomon-core")
# settings.beginGroup("modules")
# paths = settings.value("path")
# settings.endGroup()

# if paths:
#     for path in paths.split(":"):
#         sys.path.append(path)

import numpy as np

import matplotlib.pyplot as plt
import matplotlib as mpl

import vtk
from cellcomplex.property_topomesh.analysis import compute_topomesh_property
from cellcomplex.utils import array_dict
from vtk.util.numpy_support import numpy_to_vtk, vtk_to_numpy, get_vtk_array_type


from .vtk_tools import vtk_combine_polydatas
from .vtk_tools import vtk_lookuptable_from_mpl_cmap
from .vtk_tools import vertex_scalar_property_polydata, edge_scalar_property_polydata, face_scalar_property_polydata, vertex_vector_property_polydata, vertex_tensor_property_polydata

from time import time as current_time
import logging


class VtkActorTopomesh():
    def __init__(self, topomesh=None, degree=None, property_name=None):
        self.topomesh = topomesh

        self.degree = degree

        self.property_name = property_name
        self.tensor_glyph = None
        self.vector_glyph = None
        self.scale_factor = None

        self.polydata = vtk.vtkPolyData()
        self.actor = vtk.vtkActor()

    def set_topomesh(self, topomesh, degree, property_name=None):
        self.topomesh = topomesh
        self.degree = degree
        self.property_name = property_name

    def update_polydata(self, degree=2, property_name=None, tensor_glyph='ellipsoid', vector_glyph='arrow', point_radius=1., vertex='sphere'):

        if degree == 3:
            # print('-----> PROP CELL', self.polydata)
            positions = self.topomesh.wisp_property("barycenter", 0)

            compute_topomesh_property(self.topomesh, "oriented_vertices", 2)

            if self.topomesh.has_wisp_property(property_name, 3, is_computed=True):
                cell_property = self.topomesh.wisp_property(property_name, 3)
            else:
                # cell_property = array_dict(dict(zip(self.topomesh.wisps(3), self.topomesh.wisps(3))))
                cell_property = np.ones(self.topomesh.nb_wisps(2))

            cell_polydatas = []

            for cid in self.topomesh.wisps(3):
                polydata_copy = vtk.vtkPolyData()
                face_vertices = self.topomesh.wisp_property("oriented_vertices", 2).values(list(self.topomesh.borders(3, cid)))
                property_data = np.array([cell_property[cid] for cid in self.topomesh.borders(3, cid)])

                cell_polydatas += [face_scalar_property_polydata(positions, face_vertices, property_data, polydata_copy)]

            self.polydata = vtk_combine_polydatas(cell_polydatas)

        elif degree == 2:
            # print('-----> PROP FACE', self.polydata)
            positions = self.topomesh.wisp_property("barycenter", 0)

            compute_topomesh_property(self.topomesh, "oriented_vertices", 2)
            face_vertices = self.topomesh.wisp_property("oriented_vertices", 2).values()

            if self.topomesh.has_wisp_property(property_name, 2, is_computed=True):
                property_data = self.topomesh.wisp_property(property_name, 2).values()

                if property_data.ndim == 3:
                    # print('-----> TENSOR', self.polydata)
                    compute_topomesh_property(self.topomesh, 'barycenter', 2)
                    positions = self.topomesh.wisp_property("barycenter", 2)

                    if property_data.shape[1] == 3 and property_data.shape[2] == 3:
                        # print('-----> TENSOR', polydata)
                        self.polydata = vertex_tensor_property_polydata(positions, property_data, self.polydata, tensor_glyph=tensor_glyph, point_radius=point_radius)
                        # print('-->VERTEX TENSOR PROP POLYDATA', polydata, polydata.GetPointData().GetArray(0))
                if property_data.ndim == 2:
                    # print('-----> VECTOR', self.polydata)
                    compute_topomesh_property(self.topomesh, 'barycenter', 2)
                    positions = self.topomesh.wisp_property("barycenter", 2)
                    self.polydata = vertex_vector_property_polydata(positions, property_data, self.polydata, vector_glyph=vector_glyph, point_radius=point_radius)

                if property_data.ndim == 1:
                    # print('-----> SCALAR', self.polydata)
                    compute_topomesh_property(self.topomesh, 'barycenter', 2)
                    self.polydata = face_scalar_property_polydata(positions, face_vertices, property_data, self.polydata)
            else:
                # print('-----> ID', self.polydata)
                property_data = np.ones(self.topomesh.nb_wisps(2))
                self.polydata = face_scalar_property_polydata(positions, face_vertices, property_data, self.polydata)

        elif degree == 1:
            # print('-----> PROP EDGE', self.polydata)
            positions = self.topomesh.wisp_property("barycenter", 0)

            compute_topomesh_property(self.topomesh, "vertices", 1)
            edge_vertices = self.topomesh.wisp_property("vertices", 1).values()

            if self.topomesh.has_wisp_property(property_name, 1, is_computed=True):
                property_data = self.topomesh.wisp_property(property_name, 1).values()
            else:
                property_data = np.ones(self.topomesh.nb_wisps(1))

            self.polydata = edge_scalar_property_polydata(positions, edge_vertices, property_data, self.polydata)

        elif degree == 0:
            # print('-----> PROP VERTEX', self.polydata)
            positions = self.topomesh.wisp_property("barycenter", 0)
            compute_topomesh_property(self.topomesh, "oriented_vertices", 2)
            face_vertices = self.topomesh.wisp_property("oriented_vertices", 2).values()

            if self.topomesh.has_wisp_property(property_name, 0, is_computed=True):
                property_data = self.topomesh.wisp_property(property_name, 0).values()

                if property_data.ndim == 3:
                    # print('-----> TENSOR', self.polydata)
                    compute_topomesh_property(self.topomesh, 'barycenter', 2)
                    # positions = self.topomesh.wisp_property("barycenter", 2)

                    if property_data.shape[1] == 3 and property_data.shape[2] == 3:
                        # print('-----> TENSOR', polydata)
                        self.polydata = vertex_tensor_property_polydata(positions, property_data, self.polydata, tensor_glyph=tensor_glyph, point_radius=point_radius)
                        # print('-->VERTEX TENSOR PROP POLYDATA', polydata, polydata.GetPointData().GetArray(0))
                if property_data.ndim == 2:
                    # print('-----> VECTOR', self.polydata)
                    compute_topomesh_property(self.topomesh, 'barycenter', 2)
                    # positions = self.topomesh.wisp_property("barycenter", 2)
                    self.polydata = vertex_vector_property_polydata(positions, property_data, self.polydata, vector_glyph=vector_glyph, point_radius=point_radius)

                if property_data.ndim == 1:
                    # print('-----> SCALAR', self.polydata)
                    compute_topomesh_property(self.topomesh, 'barycenter', 2)
                    # positions = self.topomesh.wisp_property("barycenter", 2)
                    self.polydata = face_scalar_property_polydata(positions, face_vertices, property_data, self.polydata)
            else:
                # print('-----> ID', self.polydata)
                property_data = np.ones(self.topomesh.nb_wisps(0))
                self.polydata = vertex_scalar_property_polydata(positions, property_data, self.polydata, point_glyph=vertex, point_radius=point_radius)

        # print('-->VERTEX TENSOR END PROP POLYDATA', polydata, polydata.GetPointData().GetArray(0))

    def update_actor(self, colormap='viridis', value_range=None, opacity=1):
        # print('POLYDATA VTK ACTOR', self.polydata)
        if value_range is None:
            if self.polydata.GetCellData().GetNumberOfArrays()>0:
                scalars = vtk_to_numpy(self.polydata.GetCellData().GetArray(0))
                value_range = (np.nanmin(scalars), np.nanmax(scalars))
            elif self.polydata.GetPointData().GetNumberOfArrays()>0:
                scalars = vtk_to_numpy(self.polydata.GetPointData().GetArray(0))
                value_range = (np.nanmin(scalars), np.nanmax(scalars))
            else:
                value_range = (0, 1)

        lut = vtk_lookuptable_from_mpl_cmap(colormap, value_range)

        mapper = vtk.vtkPolyDataMapper()
        mapper.SetInputData(self.polydata)
        mapper.SetLookupTable(lut)

        if self.actor is None:
            self.actor = vtk.vtkActor()
        self.actor.SetMapper(mapper)

        self.actor.GetProperty().SetOpacity(opacity)

    def update(self, scale_glyph=1., tensor_glyph='ellipsoid', vector_glyph='lines', opacity=1, colormap='viridis', vertex='sphere'):
        self.polydata.Initialize()
        self.update_polydata(self.degree, property_name=self.property_name, point_radius=scale_glyph, tensor_glyph=tensor_glyph, vector_glyph=vector_glyph, vertex=vertex)
        self.update_actor(colormap=colormap, opacity=opacity)

    def get_actor(self):
        return self.actor

    def clean(self):
        self.polydata.Initialize()

class VtkRender():
    def __init__(self, create_renderer=True):

        self.axes_widget = None
        self.scalar_bar_widget = None
        self.actors = None
        self.assembly = None

        self.renderer = None
        self.render_window = None
        self.render_window_interactor = None

        if create_renderer:
            self.set_renderer()

        self.scalar_bar_actor = None
        self.scalar_bar_widget = None

    def set_actors(self, *actors):
        self.actors = list(actors)

    def set_renderer(self):
        self.renderer = vtk.vtkRenderer()
        self.render_window = vtk.vtkRenderWindow()
        self.render_window.AddRenderer(self.renderer)
        self.render_window.SetSize(1000, 1000)
        self.render_window_interactor = vtk.vtkRenderWindowInteractor()
        self.render_window_interactor.SetRenderWindow(self.render_window)
        self.render_window_interactor.SetInteractorStyle(vtk.vtkInteractorStyleTrackballCamera())

    def set_axes(self):
        self.axes_widget = vtk.vtkOrientationMarkerWidget()
        self.axes_widget.SetOutlineColor(1, 1, 1)
        self.axes_widget.SetOrientationMarker(vtk.vtkAxesActor())
        self.axes_widget.SetInteractor(self.render_window_interactor)
        self.axes_widget.SetViewport(0, 0, 0.2, 0.2)

    def set_colorbar(self, actor_index=0, number_labels=5, font_size=20):
        actor = self.actors[actor_index]
        if self.scalar_bar_actor is None:
            self.scalar_bar_actor = vtk.vtkScalarBarActor()
        self.scalar_bar_actor.SetOrientationToHorizontal()
        self.scalar_bar_actor.UnconstrainedFontSizeOn()
        self.scalar_bar_actor.SetMaximumWidthInPixels(60)
        self.scalar_bar_actor.SetLookupTable(actor.GetMapper().GetLookupTable())
        self.scalar_bar_actor.SetNumberOfLabels(number_labels)
        self.scalar_bar_actor.GetLabelTextProperty().SetFontSize(font_size)
        if self.scalar_bar_widget is None:
            self.scalar_bar_widget = vtk.vtkScalarBarWidget()
        self.scalar_bar_widget.SetInteractor(self.render_window_interactor)
        self.scalar_bar_widget.SetScalarBarActor(self.scalar_bar_actor)

    def clean_colorbar(self):
        self.scalar_bar_widget = None

    def set_view(self, renderer, interactor):
        self.renderer = renderer
        self.render_window_interactor = interactor

    def get_actors(self):
        if self.assembly is None:
            self.assembly = vtk.vtkAssembly()
            for act in self.actors:
                self.assembly.AddPart(act)

        return self.assembly

    def render(self, background=(0.1, 0.1, 0.1), focal_point=(1, 0, 0), view_up=(0, 0, 1)):

        if self.assembly is None:
            self.assembly = vtk.vtkAssembly()
            for actor in self.actors:
                self.assembly.AddPart(actor)
        self.renderer.AddActor(self.assembly)
        self.renderer.SetBackground(*background)
        camera = self.renderer.GetActiveCamera()
        camera.SetPosition(0, 0, 0)
        camera.SetFocalPoint(*focal_point)
        camera.SetViewUp(*view_up)
        self.renderer.ResetCamera()

        if self.scalar_bar_widget is not None:
            self.scalar_bar_widget.On()

        if self.axes_widget is not None:
            self.axes_widget.SetEnabled(1)
            self.axes_widget.InteractiveOn()

        self.render_window.Render()
        self.render_window_interactor.Start()

if __name__ == '__main__':

    from vplants.cellcomplex.property_topomesh.example_topomesh import hexagonal_grid_topomesh, hexagonal_prism_layered_grid_topomesh, circle_voronoi_topomesh
    from vplants.cellcomplex.property_topomesh.property_topomesh_extraction import star_interface_topomesh
    from vplants.cellcomplex.property_topomesh.property_topomesh_io import read_ply_property_topomesh

    # topomesh = star_interface_topomesh(hexagonal_grid_topomesh(3))
    topomesh = hexagonal_grid_topomesh(3)
    # topomesh = hexagonal_prism_layered_grid_topomesh(size=3, n_layers=2, triangulate=False)

    # topomesh.update_wisp_property('barycenter_x', 0, dict(zip(topomesh.wisps(0), topomesh.wisp_property("barycenter", 0).values(list(topomesh.wisps(0)))[:, 0])))
    #
    # compute_topomesh_property(topomesh, 'barycenter', 2)
    # topomesh.update_wisp_property('barycenter_x', 2, dict(zip(topomesh.wisps(2), topomesh.wisp_property("barycenter", 2).values()[:, 0])))
    #
    # compute_topomesh_property(topomesh, 'barycenter', 3)
    # topomesh.update_wisp_property('barycenter_x', 3, dict(zip(topomesh.wisps(3), topomesh.wisp_property("barycenter", 3).values()[:, 0])))
    #
    # compute_topomesh_property(topomesh, 'length', 1)

    # topomesh = star_interface_topomesh(circle_voronoi_topomesh(3, z_coef=4))
    tensor = np.zeros((topomesh.nb_wisps(2), 3, 3))
    tensor[:] = np.array([[1, 0, 0], [0, 1, 0], [0, 0, 0]])
    topomesh.update_wisp_property('tensor', 2, dict(zip(topomesh.wisps(2), tensor)))

    actor_topomesh = VtkActorTopomesh(topomesh, 0)
    actor_topomesh.update(vertex='sphere', scale_glyph=0.2)
    actor_property = VtkActorTopomesh(topomesh, 1)
    actor_property.update(vector_glyph='arrow', opacity=0.1)

    vtkrender = VtkRender([actor_topomesh.get_actor(), actor_property.get_actor()])
    vtkrender.set_axes()
    vtkrender.set_colorbar(1)
    vtkrender.render()
