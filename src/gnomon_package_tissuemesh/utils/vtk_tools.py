# Version: $Id$
#
#

# Commentary:
#
#

# Change Log:
#
#

# Code:

import numpy as np

import matplotlib.pyplot as plt
import matplotlib as mpl

import vtk
from cellcomplex.property_topomesh.analysis import compute_topomesh_property
from cellcomplex.utils import array_dict
from vtk.util.numpy_support import numpy_to_vtk, vtk_to_numpy, get_vtk_array_type

from time import time as current_time
import logging



def vtk_image_import_from_image(image):

    image_import = vtk.vtkImageImport()
    data_string = image.tostring('F')
    image_import.CopyImportVoidPointer(data_string, len(data_string))

    image_import.SetNumberOfScalarComponents(1)

    if image.dtype == np.uint8:
        image_import.SetDataScalarTypeToUnsignedChar()
    else:
        image_import.SetDataScalarTypeToUnsignedShort()
    image_import.GetOutput().AllocateScalars(get_vtk_array_type(image.dtype), 1)

    logging.info("--> Setting whole extent")
    image_import.SetWholeExtent(0, image.shape[0]-1, 0, image.shape[1]-1, 0, image.shape[2]-1)
    logging.info("--> Setting data extent")
    image_import.SetDataExtentToWholeExtent()
    logging.info("--> Updating")
    image_import.Modified()
    image_import.UpdateInformation()
    image_import.Update()

    return image_import


def vtk_image_data_from_image(image):

    image_data = vtk.vtkImageData()
    # image_array = numpy_to_vtk(image.transpose(1, 0, 2)[::-1, ::-1].transpose(2, 0, 1).ravel(), deep=True, array_type=get_vtk_array_type(image.dtype))
    image_array = numpy_to_vtk(image.transpose(2, 1, 0).ravel(), deep=True, array_type=get_vtk_array_type(image.dtype))
    # .transpose(2, 0, 1) may be required depending on numpy array order see - https://github.com/quentan/Test_ImageData/blob/master/TestImageData.py

    image_data.SetDimensions(image.shape)
    image_data.SetSpacing(image.voxelsize)
    image_data.SetOrigin(image.origin)
    image_data.GetPointData().SetScalars(image_array)

    logging.info("--> Image data : "+str(image_data.GetDimensions()))

    return image_data


def vtk_lookuptable_from_mpl_cmap(colormap='gray', value_range=(0, 1)):

    cmap = mpl.cm.get_cmap(colormap)

    if isinstance(cmap, mpl.colors.LinearSegmentedColormap):
        if np.all([np.all(np.sort(np.array(cmap._segmentdata[k])[:, 0])==np.array(cmap._segmentdata['red'])[:, 0]) for k in ['red', 'green', 'blue']]):
            cmap_data = np.transpose([cmap._segmentdata[k] for k in ['red', 'green', 'blue']], (1, 0, 2))[:, :, :2]
            cmap_dict = dict(zip(cmap_data[:, 0, 0], cmap_data[:, :, 1]))
        else:
            cmap_keys = np.unique(np.concatenate([np.array(cmap._segmentdata[k])[:, 0] for k in ['red', 'green', 'blue']]))
            cmap_values = mpl.cm.ScalarMappable(cmap=cmap, norm=mpl.colors.Normalize(0, 1)).to_rgba(cmap_keys)[:, :3]
            cmap_dict = dict(zip(cmap_keys, cmap_values))
    elif isinstance(cmap, mpl.colors.ListedColormap):
        cmap_dict = dict(zip(np.linspace(0, 1, len(cmap.colors)), cmap.colors))

    lut = vtk.vtkColorTransferFunction()
    lut.RemoveAllPoints()

    for val in np.sort(cmap_dict.keys()):
        node = val*value_range[1] + (1-val)*value_range[0]
        lut.AddRGBPoint(node, *cmap_dict[val])

    lut.ClampingOn()
    lut.Modified()

    return lut


def vtk_glasbey_lookuptable(values):
    from models.gnomonMplColormaps import glasbey

    lut = vtk.vtkColorTransferFunction()
    lut.RemoveAllPoints()
    for val in np.sort(values):
        lut.AddRGBPoint(val, *glasbey[int(val%256)])
    lut.ClampingOn()
    lut.Modified()

    return lut


def vtk_glyph_polydata(input_polydata, point_radius=1., tensor_glyph='crosshair', vector_glyph='arrow', point_glyph='sphere'):
    if (input_polydata.GetNumberOfCells() == 0) and (input_polydata.GetNumberOfPoints() > 0):
        print("Setting Glyph Radius : ", point_radius)

        if input_polydata.GetPointData().GetNumberOfComponents() == 1:
            glyph = vtk.vtkGlyph3D()
            if point_glyph == 'sphere':
                print('> SPHERE')
                sphere = vtk.vtkSphereSource()
                sphere.SetRadius(point_radius)
                sphere.SetThetaResolution(12)
                sphere.SetPhiResolution(12)
                sphere.Update()
                glyph.SetSourceData(sphere.GetOutput())

            elif point_glyph == 'point':
                print('> POINTS')
                point = vtk.vtkPointSource()
                point.SetRadius(0)
                point.SetNumberOfPoints(1)
                point.Update()
                glyph.SetSourceData(point.GetOutput())

            glyph.SetScaleModeToDataScalingOff()
            glyph.SetColorModeToColorByScalar()
            glyph.SetInputData(input_polydata)

        elif input_polydata.GetPointData().GetNumberOfComponents() == 3:
            glyph = vtk.vtkGlyph3D()
            if vector_glyph == 'line':
                line = vtk.vtkLineSource()
                line.SetPoint1(0, 0, 0)
                line.SetPoint2(1, 0, 0)
                line.Update()
                glyph.SetSourceConnection(line.GetOutputPort())
            elif vector_glyph == 'arrow':
                arrow = vtk.vtkArrowSource()
                arrow.SetTipLength(0.2)
                arrow.SetShaftRadius(0.05)
                arrow.SetTipRadius(0.1)
                arrow.SetTipResolution(32)
                arrow.SetShaftResolution(32)
                arrow.Update()
                glyph.SetSourceConnection(arrow.GetOutputPort())

            glyph.SetInputData(input_polydata)

            glyph.SetVectorModeToUseVector()
            glyph.SetColorModeToColorByVector()
            glyph.SetScaleModeToScaleByVector()
            glyph.SetScaleFactor(point_radius)

        elif input_polydata.GetPointData().GetNumberOfComponents() == 9:

            glyph = vtk.vtkTensorGlyph()

            if tensor_glyph == 'ellipsoid':
                sphere = vtk.vtkSphereSource()
                sphere.SetThetaResolution(12)
                sphere.SetPhiResolution(8)
                sphere.Update()
                glyph.SetSourceConnection(sphere.GetOutputPort())
                glyph.ThreeGlyphsOff()
            elif tensor_glyph == 'crosshair':
                line = vtk.vtkLineSource()
                line.SetPoint1(0, 0, 0)
                line.SetPoint2(1, 0, 0)
                line.Update()
                glyph.SetSourceConnection(line.GetOutputPort())
                glyph.ThreeGlyphsOn()
            glyph.SetInputData(input_polydata)
            glyph.ColorGlyphsOn()
            glyph.SetColorModeToEigenvalues()
            glyph.SymmetricOn()
            glyph.SetScaleFactor(point_radius)
            glyph.ExtractEigenvaluesOn()

        glyph.Update()
        polydata = glyph.GetOutput()

    else:
        polydata = input_polydata

    return polydata

def vtk_combine_polydatas(polydata_list):
    if len(polydata_list)>0:
        appender = vtk.vtkAppendPolyData()
        for polydata in polydata_list:
            appender.AddInputData(polydata)
        appender.Update()

        cleaner = vtk.vtkCleanPolyData()
        cleaner.SetInputConnection(appender.GetOutputPort())
        cleaner.Update()

        return cleaner.GetOutput()
    else:
        return vtk.vtkPolyData()

def vtk_tube_polydata(input_polydata, radius=1):

    all_lines = input_polydata.GetLines()
    all_lines.InitTraversal()
    id_list = vtk.vtkIdList()

    line_polydatas = []

    while all_lines.GetNextCell(id_list):

        points = vtk.vtkPoints()
        for i_point in range(id_list.GetNumberOfIds()):
            points.InsertPoint(i_point, input_polydata.GetPoint(id_list.GetId(i_point)))

        spline = vtk.vtkParametricSpline()
        spline.SetPoints(points)

        function_source = vtk.vtkParametricFunctionSource()
        function_source.SetParametricFunction(spline)
        function_source.SetUResolution(8*points.GetNumberOfPoints())
        function_source.Update()
        n_points = function_source.GetOutput().GetNumberOfPoints()
        # print(n_points)

        interpolated_radius = vtk.vtkTupleInterpolator()
        interpolated_radius.SetInterpolationTypeToLinear()
        interpolated_radius.SetNumberOfComponents(1)
        for i_r in range(id_list.GetNumberOfIds()):
            interpolated_radius.AddTuple(i_r, [radius])

        tube_radius = vtk.vtkDoubleArray()
        tube_radius.SetNumberOfTuples(n_points)
        tube_radius.SetName("TubeRadius")
        tMin = interpolated_radius.GetMinimumT()
        tMax = interpolated_radius.GetMaximumT()

        for i_r, i in enumerate(np.arange(n_points)/float(n_points-1)):
            r = [0.]
            interpolated_radius.InterpolateTuple(tMin + i*(tMax - tMin), r)
            tube_radius.SetTuple1(i_r, r[0])
        # print([tube_radius.GetTuple1(i_r) for i_r in range(n_points)])

        tube_polydata = function_source.GetOutput()
        tube_polydata.GetPointData().AddArray(tube_radius)
        tube_polydata.GetPointData().SetActiveScalars("TubeRadius")

        tuber = vtk.vtkTubeFilter()
        tuber.SetInputData(tube_polydata)
        tuber.SetNumberOfSides(16)
        tuber.SetVaryRadiusToVaryRadiusByAbsoluteScalar()
        tuber.Update()

        polydata = tuber.GetOutput()
        polydata.GetPointData().RemoveArray("TubeRadius")

        line_polydatas += [polydata]

    return vtk_combine_polydatas(line_polydatas)


def face_scalar_property_polydata(positions, face_vertex_ids, face_property_data, polydata=None):

    assert face_property_data.ndim == 1
    assert not face_property_data.dtype in ['O']
    assert len(face_property_data) == len(face_vertex_ids)

    unique_vertex_ids = np.unique(np.concatenate(face_vertex_ids))

    if polydata is None:
        polydata = vtk.vtkPolyData()
    vtk_points = vtk.vtkPoints()

    vtk_faces = vtk.vtkCellArray()
    vtk_face_data = vtk.vtkDoubleArray()

    double_array = numpy_to_vtk(positions.values(unique_vertex_ids), deep=True, array_type=vtk.VTK_DOUBLE)
    vtk_points.SetData(double_array)
    point_mapping = array_dict(dict(zip(unique_vertex_ids, range(len(unique_vertex_ids)))))

    for face_vids, face_property in zip(face_vertex_ids, face_property_data):
        vtk_face_point_ids = point_mapping.values(face_vids)

        vtk_face_id = vtk_faces.InsertNextCell(len(vtk_face_point_ids))
        for vtk_point_id in vtk_face_point_ids:
            vtk_faces.InsertCellPoint(vtk_point_id)
        vtk_face_data.InsertValue(vtk_face_id, face_property)

    polydata.SetPoints(vtk_points)
    polydata.SetPolys(vtk_faces)
    polydata.GetCellData().SetScalars(vtk_face_data)

    return polydata


def edge_scalar_property_polydata(positions, edge_vertex_ids, edge_property_data, polydata=None):

    assert edge_property_data.ndim == 1
    assert not edge_property_data.dtype in ['O']
    assert edge_vertex_ids.ndim == 2
    assert edge_vertex_ids.shape[1] == 2
    assert len(edge_property_data) == len(edge_vertex_ids)

    unique_vertex_ids = np.unique(np.concatenate(edge_vertex_ids))

    if polydata is None:
        polydata = vtk.vtkPolyData()
    vtk_points = vtk.vtkPoints()

    vtk_edges = vtk.vtkCellArray()
    vtk_edge_data = vtk.vtkDoubleArray()

    double_array = numpy_to_vtk(positions.values(unique_vertex_ids), deep=True, array_type=vtk.VTK_DOUBLE)
    vtk_points.SetData(double_array)
    point_mapping = array_dict(dict(zip(unique_vertex_ids, range(len(unique_vertex_ids)))))

    for edge_vids, edge_property in zip(edge_vertex_ids, edge_property_data):
        vtk_edge_point_ids = point_mapping.values(edge_vids)

        vtk_edge_id = vtk_edges.InsertNextCell(2)
        for vtk_point_id in vtk_edge_point_ids:
            vtk_edges.InsertCellPoint(vtk_point_id)
        vtk_edge_data.InsertValue(vtk_edge_id, edge_property)

    polydata.SetPoints(vtk_points)
    polydata.SetLines(vtk_edges)
    polydata.GetCellData().SetScalars(vtk_edge_data)

    return polydata


def vertex_tensor_property_polydata(positions, vertex_property_data, polydata=None, tensor_glyph='ellipsoid', point_radius=1.):

    assert vertex_property_data.ndim == 3
    assert len(vertex_property_data) == len(positions)

    if polydata is None:
        polydata = vtk.vtkPolyData()
    vtk_points = vtk.vtkPoints()

    double_array = numpy_to_vtk(positions.values(), deep=True, array_type=vtk.VTK_DOUBLE)
    vtk_points.SetData(double_array)

    property_validity = np.logical_not(np.any(np.any(np.isnan(vertex_property_data), axis=1), axis=1))

    eigval, eigvec = np.linalg.eig(vertex_property_data[property_validity])
    min_eigval = np.amin(np.abs(eigval[eigval != 0]))
    eps = min_eigval*0.01
    eigval[np.abs(eigval) < 1e-4] = eps
    vertex_property_data[property_validity] = np.einsum('ni, nji, nki->njk', eigval, eigvec, eigvec)

    vtk_point_data = numpy_to_vtk(vertex_property_data.reshape(len(positions), 9), deep=True, array_type=vtk.VTK_DOUBLE)

    # for fid, face_property in enumerate(face_property_data):
    #     vtk_point_data.InsertTuple9(fid, *np.ravel(face_property))

    polydata.SetPoints(vtk_points)
    polydata.GetPointData().SetTensors(vtk_point_data)

    return vtk_glyph_polydata(polydata, tensor_glyph=tensor_glyph, point_radius=point_radius)


def vertex_vector_property_polydata(positions, vertex_property_data, polydata=None, vector_glyph='arrow', point_radius=1.):

    assert vertex_property_data.ndim == 2
    assert len(vertex_property_data) == len(positions)

    if polydata is None:
        polydata = vtk.vtkPolyData()
    vtk_points = vtk.vtkPoints()

    double_array = numpy_to_vtk(positions.values(), deep=True, array_type=vtk.VTK_DOUBLE)
    vtk_points.SetData(double_array)

    vtk_point_data = numpy_to_vtk(vertex_property_data.reshape(len(positions), 3), deep=True, array_type=vtk.VTK_DOUBLE)

    # for fid, face_property in enumerate(face_property_data):
    #     vtk_point_data.InsertTuple9(fid, *np.ravel(face_property))

    polydata.SetPoints(vtk_points)
    polydata.GetPointData().SetVectors(vtk_point_data)

    return vtk_glyph_polydata(polydata, vector_glyph=vector_glyph, point_radius=point_radius)

def vertex_scalar_property_polydata(positions, vertex_property_data, polydata=None, point_glyph='sphere', point_radius=1.):

    assert vertex_property_data.ndim == 1
    assert not vertex_property_data.dtype in ['O']
    assert len(vertex_property_data) == len(positions)


    if polydata is None:
        polydata = vtk.vtkPolyData()
    vtk_points = vtk.vtkPoints()
    vtk_point_data = vtk.vtkDoubleArray()

    double_array = numpy_to_vtk(positions.values(), deep=True, array_type=vtk.VTK_DOUBLE)
    vtk_points.SetData(double_array)

    vtk_point_data = numpy_to_vtk(vertex_property_data, deep=True, array_type=vtk.VTK_DOUBLE)
    vtk_point_data.SetNumberOfComponents(1)

    polydata.SetPoints(vtk_points)
    polydata.GetPointData().SetScalars(vtk_point_data)

    return vtk_glyph_polydata(polydata, point_glyph=point_glyph, point_radius=point_radius)


def property_topomesh_to_vtk_polydata(topomesh, degree=2, property_name=None, tensor_glyph='ellipsoid', vector_glyph='arrow', point_radius=1.):

    if degree == 3:

        positions = topomesh.wisp_property("barycenter", 0)

        compute_topomesh_property(topomesh, "oriented_vertices", 2)

        if topomesh.has_wisp_property(property_name, 3, is_computed=True):
            cell_property = topomesh.wisp_property(property_name, 3)
        else:
            cell_property = array_dict(dict(zip(topomesh.wisps(3), topomesh.wisps(3))))

        cell_polydatas = []

        for cid in topomesh.wisps(3):
            face_vertices = topomesh.wisp_property("oriented_vertices", 2).values(list(topomesh.borders(3, cid)))
            property_data = np.array([cell_property[cid] for fid in topomesh.borders(3, cid)])

            cell_polydatas += [face_scalar_property_polydata(positions, face_vertices, property_data)]

        return vtk_combine_polydatas(cell_polydatas)

    elif degree == 2:

        positions = topomesh.wisp_property("barycenter", 0)

        compute_topomesh_property(topomesh, "oriented_vertices", 2)
        face_vertices = topomesh.wisp_property("oriented_vertices", 2).values()

        if topomesh.has_wisp_property(property_name, 2, is_computed=True):
            property_data = topomesh.wisp_property(property_name, 2).values()

            if property_data.ndim == 3:
                compute_topomesh_property(topomesh, 'barycenter', 2)
                positions = topomesh.wisp_property("barycenter", 2)

                if property_data.shape[1] == 3 and property_data.shape[2] == 3:
                    return vertex_tensor_property_polydata(positions, property_data, tensor_glyph=tensor_glyph, point_radius=point_radius)

            if property_data.ndim == 2:
                compute_topomesh_property(topomesh, 'barycenter', 2)
                positions = topomesh.wisp_property("barycenter", 2)
                return vertex_vector_property_polydata(positions, property_data, vector_glyph=vector_glyph, point_radius=point_radius)
        else:
            property_data = np.ones(topomesh.nb_wisps(2))

        return face_scalar_property_polydata(positions, face_vertices, property_data)

    elif degree == 1:

        positions = topomesh.wisp_property("barycenter", 0)

        compute_topomesh_property(topomesh, "vertices", 1)
        edge_vertices = topomesh.wisp_property("vertices", 1).values()

        if topomesh.has_wisp_property(property_name, 1, is_computed=True):
            property_data = topomesh.wisp_property(property_name, 1).values()
        else:
            property_data = np.ones(topomesh.nb_wisps(1))

        return edge_scalar_property_polydata(positions, edge_vertices, property_data)

    elif degree == 0:

        positions = topomesh.wisp_property("barycenter", 0)

        if topomesh.has_wisp_property(property_name, 0, is_computed=True):
            property_data = topomesh.wisp_property(property_name, 0).values(positions.keys())
        else:
            property_data = np.ones(topomesh.nb_wisps(0))

        return vertex_scalar_property_polydata(positions, property_data)


def image_to_vtk_cell_polydatas(img, labels=None, smoothing=2, decimation=2):

    start_time = current_time()
    logging.info("--> Generating vtk mesh from image")

    image_data = vtk_image_data_from_image(img)

    if labels is None:
        labels = np.unique(img)

    vtk_polydatas = {}

    for label in labels:

        cell_start_time = current_time()

        # cell_volume = (img==label).sum()*np.array(img.voxelsize).prod()

        contour = vtk.vtkDiscreteMarchingCubes()
        contour.SetInputData(image_data)
        contour.ComputeNormalsOn()
        contour.ComputeGradientsOn()
        contour.SetValue(0, label)

        if smoothing>0:
            smoother = vtk.vtkWindowedSincPolyDataFilter()
            smoother.SetInputConnection(contour.GetOutputPort())
            smoother.BoundarySmoothingOn()
            # smoother.FeatureEdgeSmoothingOn()
            smoother.FeatureEdgeSmoothingOff()
            smoother.SetFeatureAngle(120.0)
            smoother.SetPassBand(0.01)
            smoother.SetNumberOfIterations(smoothing)
            smoother.NonManifoldSmoothingOn()
            smoother.NormalizeCoordinatesOn()

        if decimation>0:
            # np.power(cell_volume, 1/3.)
            decimate = vtk.vtkQuadricDecimation()
            if smoothing>0:
                decimate.SetInputConnection(smoother.GetOutputPort())
            else:
                decimate.SetInputConnection(contour.GetOutputPort())
            decimate.VolumePreservationOff()
            decimate.SetTargetReduction(1. - 1/float(decimation))

        if decimation>0:
            decimate.Update()
            cell_polydata = decimate.GetOutput()
        elif smoothing>0:
            smoother.Update()
            cell_polydata = smoother.GetOutput()
        else:
            contour.Update()
            cell_polydata = contour.GetOutput()

        cell_data = vtk.vtkLongArray()
        for t in range(cell_polydata.GetNumberOfCells()):
            cell_data.InsertValue(t, label)
        cell_polydata.GetCellData().SetScalars(cell_data)

        vtk_polydatas[label] = cell_polydata

        cell_end_time = current_time()
        # logging.info("  --> Cell "+str(label)+" : "+str(cell_polydata.GetNumberOfCells())+" triangles ("+str(cell_volume)+" microm3 ) ["+str(cell_end_time-cell_start_time)+" s]")
        logging.info("  --> Cell "+str(label)+" : "+str(cell_polydata.GetNumberOfCells())+" triangles ["+str(cell_end_time-cell_start_time)+" s]")
        # logging.info("  --> Cell "+str(label)+" ["+str(cell_end_time-cell_start_time)+" s]")


    end_time = current_time()
    logging.info("<-- Generating vtk mesh from image      ["+str(end_time-start_time)+" s]")

    return vtk_polydatas

def vtk_actor(polydata, actor=None, colormap='viridis', value_range=None, opacity=1):
    if value_range is None:
        if polydata.GetCellData().GetNumberOfArrays()>0:
            scalars = vtk_to_numpy(polydata.GetCellData().GetArray(0))
            value_range = (np.nanmin(scalars), np.nanmax(scalars))
        elif polydata.GetPointData().GetNumberOfArrays()>0:
            scalars = vtk_to_numpy(polydata.GetPointData().GetArray(0))
            value_range = (np.nanmin(scalars), np.nanmax(scalars))
        else:
            value_range = (0, 1)

    lut = vtk_lookuptable_from_mpl_cmap(colormap, value_range)

    mapper = vtk.vtkPolyDataMapper()
    mapper.SetInputData(polydata)
    mapper.SetLookupTable(lut)

    if actor is None:
        actor = vtk.vtkActor()
    actor.SetMapper(mapper)

    actor.GetProperty().SetOpacity(opacity)
    return actor


def vtk_display_polydata(polydata, background=(0.1, 0.1, 0.15), focal_point=(1, 0, 0), view_up=(0, 0, 1)):
    mapper = vtk.vtkPolyDataMapper()
    mapper.SetInputData(polydata)

    actor = vtk.vtkActor()
    actor.SetMapper(mapper)

    vtk_display_actor(actor, background=background, focal_point=focal_point, view_up=view_up)


def vtk_scalar_bar_widget(actor, render_window_interactor, scalar_bar_widget=None, number_labels=10, font_size=20):
    scalar_bar = vtk.vtkScalarBarActor()
    scalar_bar.SetOrientationToHorizontal()
    scalar_bar.SetLookupTable(actor.GetMapper().GetLookupTable())
    scalar_bar.SetNumberOfLabels(number_labels)
    scalar_bar.GetLabelTextProperty().SetFontSize(font_size)
    if scalar_bar_widget is None:
        scalar_bar_widget = vtk.vtkScalarBarWidget()
    scalar_bar_widget.SetInteractor(render_window_interactor)
    scalar_bar_widget.SetScalarBarActor(scalar_bar)

    return scalar_bar_widget


def vtk_axes_actor(actor, axes_actor=None, font_size=20):
    if axes_actor is None:
        axes = vtk.vtkAxesActor()
    transform = vtk.vtkTransform()
    bounds = np.array(actor.GetBounds())
    print('--->', bounds)
    min_x = bounds[0]
    max_x = bounds[1]
    mean_x = 0.1*(max_x - min_x)
    min_y = bounds[2]
    max_y = bounds[3]
    mean_y = 0.1*(max_y- min_y)
    min_z = bounds[4]
    max_z = bounds[5]
    mean_z = 0.1*(max_z - min_z)
    max = np.max(np.array((mean_x, mean_y, mean_z)))
    axes.SetTotalLength(max, max, max)
    transform.Translate(min_x-0.5*mean_x, min_y-0.5*mean_y, min_z-0.5*mean_z)
    axes.SetUserTransform(transform)
    axes.GetXAxisCaptionActor2D().GetTextActor().SetTextScaleModeToNone()
    axes.GetYAxisCaptionActor2D().GetTextActor().SetTextScaleModeToNone()
    axes.GetZAxisCaptionActor2D().GetTextActor().SetTextScaleModeToNone()
    axes.GetXAxisCaptionActor2D().GetCaptionTextProperty().SetFontSize(font_size)
    axes.GetYAxisCaptionActor2D().GetCaptionTextProperty().SetFontSize(font_size)
    axes.GetZAxisCaptionActor2D().GetCaptionTextProperty().SetFontSize(font_size)

    return axes

def vtk_display_actor(actor, background=(0.1, 0.1, 0.1), focal_point=(1, 0, 0), view_up=(0, 0, 1), set_scalar_bar=False, set_axes=False):
    renderer = vtk.vtkRenderer()
    render_window = vtk.vtkRenderWindow()
    render_window.AddRenderer(renderer)
    render_window.SetSize(1000, 1000)
    render_window_interactor = vtk.vtkRenderWindowInteractor()
    render_window_interactor.SetRenderWindow(render_window)
    render_window_interactor.SetInteractorStyle(vtk.vtkInteractorStyleTrackballCamera())

    if set_scalar_bar:
        scalar_bar_widget = vtk_scalar_bar_widget(actor, render_window_interactor)
        scalar_bar_widget.On()

    if set_axes:
        axes = vtk_axes_actor(actor)
        renderer.AddActor(axes)

    renderer.AddActor(actor)
    renderer.SetBackground(*background)

    camera = renderer.GetActiveCamera()
    camera.SetPosition(0, 0, 0)
    camera.SetFocalPoint(*focal_point)
    camera.SetViewUp(*view_up)
    renderer.ResetCamera()

    render_window.Render()
    render_window_interactor.Start()


if __name__ == '__main__':

    from vplants.cellcomplex.property_topomesh.example_topomesh import hexagonal_grid_topomesh, hexagonal_prism_layered_grid_topomesh, circle_voronoi_topomesh
    from vplants.cellcomplex.property_topomesh.property_topomesh_extraction import star_interface_topomesh
    from vplants.cellcomplex.property_topomesh.property_topomesh_io import read_ply_property_topomesh

    topomesh = star_interface_topomesh(hexagonal_grid_topomesh(3))
    # topomesh = hexagonal_grid_topomesh(3)
    # topomesh = hexagonal_prism_layered_grid_topomesh(size=3, n_layers=2, triangulate=False)

    # topomesh.update_wisp_property('barycenter_x', 0, dict(zip(topomesh.wisps(0), topomesh.wisp_property("barycenter", 0).values(list(topomesh.wisps(0)))[:, 0])))
    #
    # compute_topomesh_property(topomesh, 'barycenter', 2)
    # topomesh.update_wisp_property('barycenter_x', 2, dict(zip(topomesh.wisps(2), topomesh.wisp_property("barycenter", 2).values()[:, 0])))
    #
    # compute_topomesh_property(topomesh, 'barycenter', 3)
    # topomesh.update_wisp_property('barycenter_x', 3, dict(zip(topomesh.wisps(3), topomesh.wisp_property("barycenter", 3).values()[:, 0])))
    #
    # compute_topomesh_property(topomesh, 'length', 1)

    # topomesh = star_interface_topomesh(circle_voronoi_topomesh(3, z_coef=4))
    tensor = np.zeros((topomesh.nb_wisps(2), 3, 3))
    tensor[:] = np.array([[1, 0, 0], [0, 1, 0], [0, 0, 0]])
    topomesh.update_wisp_property('tensor', 2, dict(zip(topomesh.wisps(2), tensor)))
    # compute_topomesh_property(topomesh, 'normal', 2, normal_method='orientation')

    polydata = property_topomesh_to_vtk_polydata(topomesh, degree=2, property_name='tensor', tensor_glyph='ellipsoid', vector_glyph='arrow', point_radius=0.5)
    actor1 = vtk_actor(polydata, colormap='viridis')
    polydata = property_topomesh_to_vtk_polydata(topomesh, degree=2)
    actor2 = vtk_actor(polydata, opacity=0.5)

    assembly = vtk.vtkAssembly()
    assembly.AddPart(actor1)
    assembly.AddPart(actor2)
    vtk_display_actor(assembly, set_scalar_bar=False, set_axes=True)

    # polydata = property_topomesh_to_vtk_polydata(topomesh, degree=3, property_name='barycenter_x')
    # polydata = property_topomesh_to_vtk_polydata(topomesh, degree=2, property_name='barycenter_x')
    # polydata = property_topomesh_to_vtk_polydata(topomesh, degree=1, property_name='length')
    # polydata = property_topomesh_to_vtk_polydata(topomesh, degree=0, property_name='barycenter_x')
    # actor = vtk_actor(polydata)

    # vtk_display_actor(actor)
