from dtkcore import d_int, d_real

import gnomon.core

from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import meshOutput

from cellcomplex.property_topomesh.example_topomesh import vtk_ellipsoid_topomesh
from cellcomplex.property_topomesh.analysis import compute_topomesh_property


@algorithmPlugin(version="1.0.0", coreversion="1.0.0")
@meshOutput('mesh_out', data_plugin="gnomonMeshDataPropertyTopomesh")
class ellipsoidMeshConstructor(gnomon.core.gnomonAbstractMeshConstructor):
    """Create an ellipsoid mesh by refinement of an icosahedron.


    """

    def __init__(self):
        super().__init__()

        self._parameters = {}
        self._parameters['radius'] = d_real('Radius', 10, 0, 1000, 0, "The radius of the ellipsoid main axis")
        self._parameters['aspect_ratio_1'] = d_real('Aspect ratio 1', 1, 0, 1, 2, "The aspect ratio relatively to the second axis")
        self._parameters['aspect_ratio_2'] = d_real('Aspect ratio 2', 1, 0, 1, 2, "The aspect ratio relatively to the third axis")
        self._parameters['subdivisions'] = d_int('Subdivisions', 3, 0, 10, "The number of mesh subdivisions steps")
        self.mesh_out = {}

    def run(self):
        self.mesh_out = {}

        topomesh = vtk_ellipsoid_topomesh(ellipsoid_radius=self['radius'],
                                          ellipsoid_scales=[self['aspect_ratio_1'], self['aspect_ratio_2'], 1],
                                          subdivisions=self['subdivisions'])

        for degree in [1, 2, 3]:
            compute_topomesh_property(topomesh, 'barycenter', degree)

        for degree in [0, 1, 2, 3]:
            positions = topomesh.wisp_property('barycenter', degree)
            for k, dim in enumerate(['x', 'y', 'z']):
                topomesh.update_wisp_property('barycenter_'+dim, degree, dict(zip(positions.keys(), positions.values()[:, k])))


        self.mesh_out[0] = topomesh
