from dtkcore import d_int, d_real

import gnomon.core

from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import cellComplexOutput

from cellcomplex.property_topomesh.example_topomesh import circle_voronoi_topomesh
from cellcomplex.property_topomesh.analysis import compute_topomesh_property

@algorithmPlugin(version="1.0.0", coreversion="1.0.0")
@cellComplexOutput('topomesh', data_plugin="gnomonCellComplexDataPropertyTopomesh")
class circleVoronoiCellComplex(gnomon.core.gnomonAbstractCellComplexConstructor):
    """Create a cellular complex as a Voronoi diagram on a circle domain.


    """

    def __init__(self):
        super().__init__()

        self._parameters = {}
        self._parameters['n_layers'] = d_int('Layers', 3, 0, 10, "Number of cell layers within the circle")
        self._parameters['cell_size'] = d_real('Cell size', 1, 0, 10, 1, "The characterisitc size of cells")
        self._parameters['circle_points'] = d_int('Circle points', 100, 10, 1000, "Number of points used to define the circle contour")

        self.topomesh = {}

    def run(self):
        self.topomesh = {}

        topomesh = circle_voronoi_topomesh(size=self['n_layers'],
                                           cell_size=self['cell_size'],
                                           circle_size=self['circle_points'],
                                           z_coef=0.,
                                           return_triangulation=False)

        for degree in [1, 2, 3]:
            compute_topomesh_property(topomesh, 'barycenter', degree)

        for degree in [0, 1, 2, 3]:
            positions = topomesh.wisp_property('barycenter', degree)
            for k, dim in enumerate(['x', 'y', 'z']):
                topomesh.update_wisp_property('barycenter_'+dim, degree, dict(zip(positions.keys(), positions.values()[:, k])))

        self.topomesh[0] = topomesh
