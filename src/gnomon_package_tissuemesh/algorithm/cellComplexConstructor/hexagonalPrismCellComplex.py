from dtkcore import d_bool, d_int, d_real

import gnomon.core

from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import cellComplexOutput

from cellcomplex.property_topomesh.example_topomesh import hexagonal_prism_layered_grid_topomesh
from cellcomplex.property_topomesh.analysis import compute_topomesh_property

@algorithmPlugin(version="1.0.0", coreversion="1.0.0")
@cellComplexOutput('topomesh', data_plugin="gnomonCellComplexDataPropertyTopomesh")
class hexagonalPrismCellComplex(gnomon.core.gnomonAbstractCellComplexConstructor):
    """Create a complex formed by layers of concentric hexagonal prisms.
s

    """

    def __init__(self):
        super().__init__()

        self._parameters = {}
        self._parameters['size'] = d_int('Size', 2, 0, 10, "Number of concentric layers of hexagonal cells")
        self._parameters['n_layers'] = d_int('Layers', 1, 0, 5, "Number of superimposed hexagonal cells")
        self._parameters['cell_size'] = d_real('Cell size', 1, 0, 10, 1, "The characteristic size of cells")
        self._parameters['height_factor'] = d_real('Height factor', 0.66, 0, 2, 2, "Ratio between the height and radius of the cells")
        self._parameters['triangulate'] = d_bool('Triangulate', True, "Whether to triangulate the faces of the complex")

        self.topomesh = {}

    def run(self):
        self.topomesh = {}

        topomesh = hexagonal_prism_layered_grid_topomesh(size=self['size'],
                                                         n_layers=self['n_layers'],
                                                         radius=self['cell_size'],
                                                         height_factor=self['height_factor'],
                                                         triangulate=self['triangulate'],
                                                         smoothing=False)

        for degree in [1, 2, 3]:
            compute_topomesh_property(topomesh, 'barycenter', degree)

        for degree in [0, 1, 2, 3]:
            positions = topomesh.wisp_property('barycenter', degree)
            for k, dim in enumerate(['x', 'y', 'z']):
                topomesh.update_wisp_property('barycenter_'+dim, degree, dict(zip(positions.keys(), positions.values()[:, k])))

        self.topomesh[0] = topomesh
