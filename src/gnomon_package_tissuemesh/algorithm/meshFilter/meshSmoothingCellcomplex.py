from copy import deepcopy

from dtkcore import d_int, d_inliststring

import gnomon.core

from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import meshInput, meshOutput

from cellcomplex.property_topomesh.optimization import property_topomesh_vertices_deformation
from cellcomplex.property_topomesh.analysis import compute_topomesh_property


@algorithmPlugin(version="1.0.0", coreversion="1.0.0")
@meshInput('mesh_in', data_plugin="gnomonMeshDataPropertyTopomesh")
@meshOutput('mesh_out', data_plugin="gnomonMeshDataPropertyTopomesh")
class meshSmoothingCellcomplex(gnomon.core.gnomonAbstractMeshFilter):
    """
    Apply an iterative optimization process to smooth a triangular mesh.

    """

    def __init__(self):
        super().__init__()

        self._parameters = {}
        self._parameters['method'] = d_inliststring('Method', "laplacian", ["laplacian", "taubin"], "Smoothing method to apply to the mesh")
        self._parameters['iterations'] = d_int('Iterations', 10, 1, 100, "The number of steps in the optimization process")

        self.mesh_in = {}
        self.mesh_out = {}

    def run(self):
        self.set_max_progress(3*len(self.mesh_in))
        self.mesh_out = {}

        for time in self.mesh_in.keys():
            self.set_progress_message(f"T {time} : Copying mesh")
            in_topomesh = self.mesh_in[time]
            out_topomesh = deepcopy(in_topomesh)
            self.increment_progress()

            self.set_progress_message(f"T {time} : Smoothing mesh ({self['iterations']} iterations)")
            omega_forces = {}
            if self['method'] == 'laplacian':
                omega_forces['laplacian_smoothing'] = 0.33
            elif self['method'] == 'taubin':
                omega_forces['taubin_smoothing'] = 0.65
            property_topomesh_vertices_deformation(out_topomesh,
                                                   omega_forces=omega_forces,
                                                   iterations=self['iterations'])
            self.increment_progress()

            self.set_progress_message(f"T {time} : Updating mesh")
            for degree in [1, 2, 3]:
                compute_topomesh_property(out_topomesh, 'barycenter', degree)

            for degree in [0, 1, 2, 3]:
                positions = out_topomesh.wisp_property('barycenter', degree)
                for k, dim in enumerate(['x', 'y', 'z']):
                    out_topomesh.update_wisp_property(
                        'barycenter_'+dim, degree,
                        dict(zip(positions.keys(), positions.values()[:, k]))
                    )
            self.increment_progress()

            self.mesh_out[time] = out_topomesh
