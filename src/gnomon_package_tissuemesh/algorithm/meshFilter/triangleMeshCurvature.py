from copy import deepcopy

from dtkcore import d_int, d_real

import gnomon.core

from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import meshInput, meshOutput

from cellcomplex.property_topomesh.analysis import compute_topomesh_property, compute_topomesh_vertex_property_from_faces, is_triangular


@algorithmPlugin(version="1.0.0", coreversion="1.0.0")
@meshInput('topomesh', data_plugin="gnomonMeshDataPropertyTopomesh")
@meshOutput('out_topomesh', data_plugin="gnomonMeshDataPropertyTopomesh")
class triangleMeshCurvature(gnomon.core.gnomonAbstractMeshFilter):
    """
    Compute principal, mean and gaussian curvatures on a triangular mesh.

    """

    def __init__(self):
        super().__init__()

        self._parameters = {}
        self._parameters['neighborhood'] = d_int('Neigborhood', 3, 1, 7, "Topological radius used for the vertex normal estimation")
        self._parameters['adjacency_sigma'] = d_real('Sigma', 1.2, 0, 5, 2, "Standard deviation used for the vertex normal estimation (in topological distance)")

        self.topomesh = {}
        self.out_topomesh = {}

    def run(self):
        self.set_max_progress(5*len(self.topomesh))
        self.out_topomesh = {}

        for time in self.topomesh.keys():
            self.set_progress_message(f"T {time} : Copying mesh")
            topomesh = self.topomesh[time]
            out_topomesh = deepcopy(topomesh)
            self.increment_progress()

            if is_triangular(out_topomesh):
                self.set_progress_message(f"T {time} : Computing face normals")
                compute_topomesh_property(out_topomesh, 'normal', 2, normal_method='orientation')
                self.increment_progress()

                self.set_progress_message(f"T {time} : Estimating vertex normals")
                compute_topomesh_vertex_property_from_faces(out_topomesh, 'normal', neighborhood=self['neighborhood'], adjacency_sigma=self['adjacency_sigma'])
                self.increment_progress()

                self.set_progress_message(f"T {time} : Computing triangle curvature tensor")
                compute_topomesh_property(out_topomesh, 'mean_curvature', 2)
                self.increment_progress()

                self.set_progress_message(f"T {time} : Averaging vertex curvature properties")
                for property_name in ['principal_curvature_min', 'principal_curvature_max']:
                    compute_topomesh_vertex_property_from_faces(out_topomesh, property_name, neighborhood=self['neighborhood'], adjacency_sigma=self['adjacency_sigma'])
                out_topomesh.update_wisp_property(
                    'mean_curvature', 0, {
                        v: (out_topomesh.wisp_property('principal_curvature_min', 0)[v] + out_topomesh.wisp_property('principal_curvature_max', 0)[v])/2
                        for v in out_topomesh.wisps(0)
                    }
                )
                out_topomesh.update_wisp_property(
                    'gaussian_curvature', 0, {
                        v: out_topomesh.wisp_property('principal_curvature_min', 0)[v] * out_topomesh.wisp_property('principal_curvature_max', 0)[v]
                        for v in out_topomesh.wisps(0)
                    }
                )
                self.increment_progress()
            else:
                self.increment_progress(4)

            self.out_topomesh[time] = out_topomesh
