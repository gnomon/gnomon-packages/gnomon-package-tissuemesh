from copy import deepcopy

from dtkcore import d_bool, d_real, d_string, d_inliststring

import gnomon.core
from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import meshInput, meshOutput

import numpy as np

from cellcomplex.property_topomesh.segmentation import topomesh_vertex_property_watershed_segmentation, topomesh_merge_vertex_watershed_regions

@algorithmPlugin(version="1.0.0", coreversion="1.0.0", name="Vertex Property Watershed")
@meshInput('topomesh', data_plugin="gnomonMeshDataPropertyTopomesh")
@meshOutput('out_topomesh', data_plugin="gnomonMeshDataPropertyTopomesh")
class vertexPropertyWatershedSegmentation(gnomon.core.gnomonAbstractMeshFilter):
    """
    Label the mesh vertices by performing a watershed on a given property.

    The method starts by identifying locally minimal / maximal vertices of the
    smoothed property that are used as seeds for a mesh watershed. The regions
    obtained from the watershed can then be merged together based on a depth
    criterion.

    """

    def __init__(self):
        super().__init__()

        self._parameters = {}
        self._parameters['property'] = d_inliststring("Property", "", [""], "Vertex property to use to perform the segmentation")
        self._parameters['region_type'] = d_inliststring("Region Type", "minima", ["maxima", "minima"], "Whether to segment maximal or minimal regions")
        self._parameters['gaussian_sigma'] = d_real("Gaussian Sigma", 5., 0, 100, 2, "Standard deviation of the Gaussian kernel used to smooth the property for seed detection")
        self._parameters['merge_regions'] = d_bool("Merge Regions", True, "Whether to merge segmented regions after watershed")
        self._parameters['merge_threshold'] = d_real("Merge Threshold", 1., 0., 10., 3, "Threshold used to merge watershed regions")
        self._parameters['label_property'] = d_string("Label Property", "segmentation", "Name to give to the property corresponding to the segmentation labels")

        self.topomesh = {}
        self.out_topomesh = {}

        self.connectParameter("property")

    def refreshParameters(self):
        if len(self.topomesh) > 0:
            topomesh = list(self.topomesh.values())[0]
            property_name = self['property']
            prop = list(topomesh.wisp_property_names(0))
            prop = [p for p in prop if topomesh.has_wisp_property(p, 0, is_computed=True)]
            prop = [p for p in prop if topomesh.wisp_property(p, 0).values().dtype != np.dtype('O')]
            prop = [p for p in prop if topomesh.wisp_property(p, 0).values().ndim == 1]
            self._parameters['property'].setValues(prop)
            self._parameters['property'].setValue(property_name if property_name in prop else prop[0])

    def onParameterChanged(self, parameter_name):
        if parameter_name == 'property':
            self._update_threshold()

    def _update_threshold(self):
        if len(self.topomesh) > 0:
            topomesh = list(self.topomesh.values())[0]
            property_values = topomesh.wisp_property(self['property'], 0).values(list(topomesh.wisps(0)))

            min_p = float(np.around(np.nanmin(property_values), decimals=3))
            max_p = float(np.around(np.nanmax(property_values), decimals=3))
            std_p = float(np.around(np.nanstd(property_values), decimals=3))
            print(min_p, max_p, std_p)

            self._parameters['merge_threshold'].setMin(0.)
            self._parameters['merge_threshold'].setMax(max_p-min_p)
            self._parameters['merge_threshold'].setValue(std_p)

    def run(self):
        self.set_max_progress((3+self['merge_regions'])*len(self.topomesh))
        self.out_topomesh = {}

        for time in self.topomesh.keys():
            self.set_progress_message(f"T {time} : Copying mesh")
            topomesh = self.topomesh[time]
            out_topomesh = deepcopy(topomesh)
            self.increment_progress()

            property_name = self['property']
            gaussian_sigma= self['gaussian_sigma']
            depth_threshold = self['merge_threshold']

            self.set_progress_message(f"T {time} : Performing watershed segmentation on {property_name}")
            topomesh_vertex_property_watershed_segmentation(
                out_topomesh,
                property_name,
                minima=(self['region_type'] == 'minima'),
                labelchoice='most',
                weighting='uniform',
                gaussian_sigma=gaussian_sigma
            )
            self.increment_progress()

            if self['merge_regions']:
                self.set_progress_message(f"T {time} : Merging watershed regions with threshold = {depth_threshold}")
                topomesh_merge_vertex_watershed_regions(
                    out_topomesh,
                    property_name,
                    minima=(self['region_type'] == 'minima'),
                    depth_threshold=depth_threshold,
                    engulfed_fraction=1.
                )
                self.increment_progress()

            self.set_progress_message(f"T {time} : Updating mesh")
            out_topomesh.update_wisp_property(
                self['label_property'], 0,
                out_topomesh.wisp_property(f'{property_name}_regions', 0).to_dict()
            )
            self.increment_progress()

            self.out_topomesh[time] = out_topomesh
