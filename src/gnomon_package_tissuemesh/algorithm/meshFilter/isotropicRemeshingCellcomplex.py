from copy import deepcopy

from dtkcore import d_int, d_real, d_bool

import gnomon.core

from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import meshInput, meshOutput

from cellcomplex.property_topomesh.optimization import property_topomesh_isotropic_remeshing
from cellcomplex.property_topomesh.analysis import compute_topomesh_property


@algorithmPlugin(version="1.0.0", coreversion="1.0.0")
@meshInput('mesh_in', data_plugin="gnomonMeshDataPropertyTopomesh")
@meshOutput('mesh_out', data_plugin="gnomonMeshDataPropertyTopomesh")
class isotropicRemeshingCellcomplex(gnomon.core.gnomonAbstractMeshFilter):
    """
    Apply an iterative remeshing algorithm to the triangular mesh.

    """

    def __init__(self):
        super().__init__()

        self._parameters = {}
        self._parameters['iterations'] = d_int('Iterations', 5, 1, 100, "The number of steps in the optimization process")
        self._parameters['target_length'] = d_real('Target length', 5., 0., 10., 1, "The target length for the mesh edges after remeshing")
        self._parameters['use_collapse'] = d_bool('Collapse', False, "Whether to use a collapse step in the remeshing algortihm")

        self.mesh_in = {}
        self.mesh_out = {}

    def run(self):
        self.set_max_progress((2+self['iterations'])*len(self.mesh_in))
        self.mesh_out = {}

        for time in self.mesh_in.keys():
            self.set_progress_message(f"T {time} : Copying mesh")
            in_topomesh = self.mesh_in[time]
            out_topomesh = deepcopy(in_topomesh)
            self.increment_progress()

            for iteration in range(self['iterations']):
                self.set_progress_message(f"T {time} : Performing remeshing iteration {iteration+1}/{self['iterations']}")
                out_topomesh = property_topomesh_isotropic_remeshing(
                    out_topomesh,
                    maximal_length=4/3*self['target_length'],
                    minimal_length=3/4*self['target_length'],
                    collapse=self['use_collapse'],
                    iterations=1
                )
                self.increment_progress()

            self.set_progress_message(f"T {time} : Updating mesh")
            for degree in [1, 2, 3]:
                compute_topomesh_property(out_topomesh, 'barycenter', degree)

            for degree in [0, 1, 2, 3]:
                positions = out_topomesh.wisp_property('barycenter', degree)
                for k, dim in enumerate(['x', 'y', 'z']):
                    out_topomesh.update_wisp_property(
                        'barycenter_'+dim, degree,
                        dict(zip(positions.keys(), positions.values()[:, k]))
                    )
            self.increment_progress()

            self.mesh_out[time] = out_topomesh
