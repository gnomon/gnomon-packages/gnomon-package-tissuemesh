# -*- python -*-
# -*- coding: utf-8 -*-
#
#
#       Copyright 2018 INRIA
#
#       File author(s):
#           Olvier Ali <olivier.ali@inria.fr>
#
#       See accompanying file LICENSE.txt
# ------------------------------------------------------------------------------
import os
import logging

import fenics as fnx
import numpy as np

from dolfin import MeshEditor

from dtkcore import d_int, d_real,  d_inliststring, d_string

import gnomon.core
from gnomon.core import gnomonAbstractFemSolver
from gnomon.core import gnomonMesh, gnomonMeshSeries

from gnomon.utils import load_plugin_group, algorithmPlugin


##############################
# -- Main class
##############################

# this line enables the same numbering between the dofs and the vectors...
fnx.parameters["reorder_dofs_serial"] = False

@algorithmPlugin(version="1.0.0", coreversion="1.0.0")
class gnomonVariationalProblem(gnomonAbstractFemSolver):

    def __init__(self):
        super().__init__()
        self._parameters = {}
        self._parameters['element_type'] = d_inliststring('Element type', 'P', ['P'], "Type of finite elements")
        self._parameters['element_order'] = d_int('Order', 1, 1, 5, "Degree of polynomials")
        self._parameters['source_term'] = d_real('Source', 0, -10, 10, 2, "Source term")
        self._parameters['bound_cond_type'] = d_inliststring('Boundary condition', 'Dirichlet', ['Dirichlet', 'Neumann'], "Type of boundary condition")
        self._parameters['bound_cond_expression'] = d_string('Expression', '1 + x[0]*x[0] + 2*x[1]*x[1]', "Expression")
        self._parameters['variational_problem'] = d_string('Problem', 'fnx.dot(fnx.grad(u), fnx.grad(v)) * fnx.dx - f * v * fnx.dx', "Variational Problem")
        self._flag_bnd = True
        self._flag_vf = True

        self.mesh                    = None
        self.initial_mesh            = None
        self.fnx_mesh                = None

        self.function_space          = None

        self.bound_conditions        = None
        self.function_bound_cond     = None

        self.tpmsh_2_fnx_pts_mapping = None
        self.tpmsh_2_fnx_tri_mapping = None
        self.fnx_2_tpmsh_pts_mapping = None
        self.fnx_2_tpmsh_tri_mapping = None


    def setMesh(self, mesh_series):
        self.initial_mesh = mesh_series.current().asMesh()

    def topomesh_2_fenics_mesh_conversion(self, topomesh):
        '''converts a topomesh into a mesh usable by the fenics module.

        Args:
        - topomesh (PropertyTopomesh): The topomesh we want to use to solve our differential equation.

        Returns:
        - fnx_mesh (fenics.Mesh): The Same mesh in the dedicated fenics format.
        - topomesh_vertices_2_fenics_pts (dict(Int)): Dict that relates topomesh ids (keys
                to the corresponding index in the fenics mesh (values.
        - topomesh_faces_2_fenics_triangles (dict(Int)): The same but for triangles.
        - fenics_pts_2_topomesh_vertices (dict(Int)): Inverse dict of topomesh_vertices_2_fenics_pts:
                Relates the pts ids of the fenics mesh (keys) into corresponding vids of the topomesh (values
        - fenics_triangles_2_topomesh_faces (dict(Int)): The same but for triangles.
        '''
        fnx_mesh = fnx.Mesh()

        editor = MeshEditor()

        editor.open(fnx_mesh, 'triangle', 2, 3)
        editor.init_vertices(topomesh.nb_wisps(0))  # number of vertices
        editor.init_cells(topomesh.nb_wisps(2))  # number of cells

        topomesh_vertices_2_fenics_pts = {}
        fenics_pts_2_topomesh_vertices = {}
        for i, vid in enumerate(topomesh.wisps(0)):
            editor.add_vertex(i, np.array(topomesh.wisp_property('barycenter', 0)[vid]))
            topomesh_vertices_2_fenics_pts[vid] = i
            fenics_pts_2_topomesh_vertices[i] = vid

        topomesh_faces_2_fenics_triangles = {}
        fenics_triangles_2_topomesh_faces = {}
        for i, tid in enumerate(topomesh.wisps(2)):
            list_indices_pts_fencis = map(lambda x: topomesh_vertices_2_fenics_pts[x], list(topomesh.borders(2, tid, 2)))

            editor.add_cell(i, np.array(list_indices_pts_fencis, dtype=np.uintp))
            topomesh_faces_2_fenics_triangles[tid] = i
            fenics_triangles_2_topomesh_faces[i] = tid

        editor.close()

        return fnx_mesh, topomesh_vertices_2_fenics_pts, topomesh_faces_2_fenics_triangles, fenics_pts_2_topomesh_vertices, fenics_triangles_2_topomesh_faces

    def setFnxMeshFunctionSpaceAndMapping(self, topomesh):
        '''Sets the fenics mesh & the function space needed for the FEM.

        Args:
        - topomesh (PropertyTopomesh): The propertyTopomesh used to generate the fenics mesh.

        Returns:
            technically nothing but initializes some parameters:
        - self.topomesh           (PropertyTopomesh): the stored topomesh.
        - self.fnx_mesh                (fenics.Mesh): the fenics version.
        - self.function_space (fenics.FunctionSpace): The function space on which we gonna estimate the solutions of our equation.
        - self.tpmsh_vids_2_fnx_pids     (dict(Int)): Dict that relates topomesh ids (keys()) to the corresponding index in the fenics mesh (values()).
        - self.tpmsh_fids_2_fnx_tids     (dict(Int)): The same but for triangles.
        '''

        self.topomesh = topomesh

        self.fnx_mesh = self.topomesh_2_fenics_mesh_conversion(self.topomesh)[0]
        self.tpmsh_2_fnx_pts_mapping = self.topomesh_2_fenics_mesh_conversion(self.topomesh)[1]
        self.tpmsh_2_fnx_tri_mapping = self.topomesh_2_fenics_mesh_conversion(self.topomesh)[2]
        self.fnx_2_tpmsh_pts_mapping = self.topomesh_2_fenics_mesh_conversion(self.topomesh)[3]
        self.fnx_2_tpmsh_tri_mapping = self.topomesh_2_fenics_mesh_conversion(self.topomesh)[4]

        self.function_space = fnx.FunctionSpace(self.fnx_mesh, self['element_type'], self['element_order'])

    def setBoundaryConditions(self):
        '''Defines boundary conditions of the morphogen field.

        This method enforces Dirichlet boundary conditions (i.e. function value on the boundary)
        N.B.: the setFnxMeshFunctionSpaceAndMapping method should be run prior to it.

        '''

        bc_expr = self['bound_cond_expression'].value()
        self._flag=True

        try:
            bc_const = float(bc_expr)
        except ValueError:

            try:
                x = {}
                x[0]=0
                x[1]=0
                eval(bc_expr)
            except SyntaxError:
                self._flag=False
                bc_expr='0'
                logging.warning('Error Syntax func_bound_expression')

            class DefExpression(fnx.Expression):
                def eval(self, value, x):
                    value[0] = eval(bc_expr)

            self.function_bound_cond = DefExpression(element=self.function_space.ufl_element())

        else:
            self.function_bound_cond = fnx.Constant(bc_const)


        if self['bound_cond_type'] == 'Dirichlet':
            #print " ---> Dirichlet Condition ", bc_expr
            def boundary(x, on_boundary):
                return on_boundary

            self.bound_conditions = fnx.DirichletBC(self.function_space, self.function_bound_cond, boundary)


        elif self['bound_cond_type'] == 'Neumann':
            #print " ---> Neumann Condition", bc_expr

            class bnd_neumann(fnx.SubDomain):
                def inside(self, x, on_boundary):
                    return on_boundary

            neumann = bnd_neumann()
            boundaries = fnx.MeshFunction('size_t', self.fnx_mesh, 1)
            boundaries.set_all(0)
            neumann.mark(boundaries, 1)

            ds = fnx.Measure('ds', domain=self.fnx_mesh, subdomain_data=boundaries)


        else:
            print("Wrong boundary condition type.")



    def setVariationalProblem(self):
        '''Defines the variational problem to solve.
        '''

        u = fnx.TrialFunction(self.function_space)
        v = fnx.TestFunction(self.function_space)
        f = fnx.Constant(self['source_term'])

        try:
            self._flag_vf = True
            eval(self['variational_problem'].value())
            F = eval(self['variational_problem'].value())
            self.lhs_expression = fnx.lhs(F)
            self.rhs_expression = fnx.rhs(F)
        except SyntaxError:
            self._flag_vf = False
            F=u*v*fnx.dx # Dummy variational form
            logging.warning('Error syntax variational_problem')
        except (RuntimeError, fnx.dolfin.UFLException):
            self._flag_vf = False
            F=u*v*fnx.dx # Dummy variational form
            logging.warning('Wrong definition of variational problem')





        # -- Defining the function used to solve the differential equation at stake.
        self.trial_function = fnx.Function(self.function_space)


    def reset(self):
        """(Re)set a simulation.
        """

        self.mesh = self.initial_mesh
        self.setFnxMeshFunctionSpaceAndMapping(self.mesh.data()._topomesh)

        self.setBoundaryConditions()
        self.setVariationalProblem()

        # self.render()


    def step(self):
        """Estimate once the variational equation over an incremental time interval.

        Args:
        - self

        Returns:
        - None
        """

        # -- Compute solution
        if self['bound_cond_type']=='Dirichlet' :
            fnx.solve(self.lhs_expression == self.rhs_expression, self.trial_function, self.bound_conditions)
        elif self['bound_cond_type']=='Neumann' :
            fnx.solve(self.lhs_expression == self.rhs_expression, self.trial_function)

        # -- Updating the value of the scalar field.
        mapping = self.tpmsh_2_fnx_pts_mapping

        new_scalar_field = {vid: self.trial_function.compute_vertex_values(self.fnx_mesh)[mapping[vid]] for vid in
                            self.topomesh.wisps(0)}

        if 'scalar_field' in self.topomesh.wisp_properties(0).keys():
            self.topomesh.update_wisp_property('scalar_field', 0, new_scalar_field)
        else:
            self.topomesh.add_wisp_property('scalar_field', 0, new_scalar_field)

        self.mesh.data().set_property_topomesh(self.topomesh)


    def run(self):
        """Solve the diffusion equation over a time interval.

        Args:
        - self

        Returns:
        - None
        """
        self.reset()

        if self._flag_bnd and self._flag_vf:
            self.step()



    def updatedMesh(self):
        """Returns the results of the simulation.

        Args:
        - self

        Returns:
        - mesh (gnomonMesh): the mesh updated with the scalar_field values after diffusion.
        """

        mesh_series = gnomonMeshSeries()
        mesh_series.insert(0, self.mesh)
        mesh_series.this.disown()

        return mesh_series




#
## gnomonVariationalProblem.py ends here.
