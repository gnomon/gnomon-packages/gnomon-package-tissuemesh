import vtk
from vtk import vtkTextActor

# from PyQt5.QtCore import Qt

from gnomon.visualization import gnomonInteractorStyle


class gnomonInteractorStyleCellComplexFaceProperty(gnomonInteractorStyle):
    def __init__(self):

        super().__init__()

        # self.view = None
        self.renderer = None
        self.textActor = None
        # self.
        # self.property_name = None
        self.topomesh = None
        self.degree = 2
        # self.property_actor = property_actor

        self.visu = None

        self.picker = vtk.vtkCellPicker()
        self.picker.SetTolerance(0.005)

        self.vtkId = -1
        self.wispId = 0
        self.click = 0

        # self.AddObserver("LeftButtonPressEvent", self.leftButtonDownEvent)
        # self.AddObserver("LeftButtonReleaseEvent", self.leftButtonUpEvent)
        # self.AddObserver("MouseMoveEvent", self.mouseMouveEvent)


    def OnLeftButtonDown(self):
        super().OnLeftButtonDown()
        self.click += 1

    def OnLeftButtonUp(self):
        super().OnLeftButtonUp()
        if self.click == 1:
            interactor = super().GetDefaultRenderer().GetRenderWindow().GetInteractor()
            clickPos = interactor.GetEventPosition()
            self.picker.Pick(clickPos[0], clickPos[1], 0, super().GetDefaultRenderer())
            self.vtkId = self.picker.GetCellId()
            self.updateTextActor()
            # self.updatePane()

        if self.click == 2 and self.vtkId > -1:
            # self.updatePane()
            # if not self.visu.view().infoPane().isToggled():
            #     self.visu.view().infoPane().toggle()
            self.click = 0
        elif self.vtkId == -1:
            self.click = 0

    def OnMouseMove(self):
        super().OnMouseMove()
        self.click = 0
        # self.visu.render()

    def updateTextActor(self):
        self.renderer = self.visu.renderer3D
        text = self.visu['property_name']+' : '

        if self.textActor is None:
            self.textActor = vtkTextActor()
            self.textActor.SetPosition2(40, 40)
            self.textActor.GetTextProperty().SetFontSize(24)
            self.textActor.GetTextProperty().SetColor(1, 1, 1)
            self.renderer.AddActor2D(self.textActor)
            self.textActor.SetInput(text)

        if self.vtkId > -1:
            value_property = self.cellId()
            if value_property != "":
                self.textActor.SetInput('        Cell : '+str(self.wispId)+', '+text+str(value_property))
            else:
                self.textActor.SetInput('        Cell : '+str(self.wispId))
        elif self.vtkId == -1:
            self.textActor.SetInput('')

        self.visu.render()

    def cellId(self):
        print("cellId")
        if self.degree == 3:
            wispIds = self.visu.cell_actor3D.polydata.GetCellData().GetArray("WispId")
        elif self.degree == 2:
            wispIds = self.visu.property_actor.polydata.GetCellData().GetArray("WispId")
        print(wispIds)

        if self.vtkId < wispIds.GetNumberOfValues():
            self.wispId = wispIds.GetValue(self.vtkId)
            property_name = self.visu['property_name']
            topomesh = self.topomesh
            if topomesh.has_wisp_property(property_name, self.degree):
                return topomesh.wisp_property(property_name, self.degree)[self.wispId]
            else:
                return ""
        else:
            self.wispId = -1
            return ""

    def updatePane(self):
        # self.view.infoPane().setTitle(self.property_name)
        # properties = self.topomesh
        # self.visu.view().infoPane().clear()
        # topomesh = self.visu.topomesh
        # degree = 2
        # properties = topomesh.wisp_property_names(degree)
        # info = {}
        # for key in properties:
        #     if self.wispId in topomesh.wisps(degree):
        #         info[key+':'] = str(topomesh.wisp_property(key, degree)[self.wispId])
        # if self.vtkId > -1:
        #     self.visu.view().infoPane().addInfoPaneItem('Cell :'+str(self.wispId), info)
        # elif self.vtkId == -1:
        #     if self.visu.view().infoPane().isToggled():
        #         self.visu.view().infoPane().toggle()
        #         self.visu.view().infoPane().clear()
        # self.view.infoPane().toggle()
        pass

    def setVisualization(self, visu):
        self.visu = visu

    def disable(self):
        super().disable()
        if self.textActor is not None:
            self.renderer.RemoveActor2D(self.textActor)

    def icon(self):
        return 0xf05b #fa::crosshairs

    def description(self):
        return "CellComplex Picker"

    def keyMap(self):
        keymap = {}
        # keymap[Qt.Key_R] = "Reset camera focus"
        return keymap
