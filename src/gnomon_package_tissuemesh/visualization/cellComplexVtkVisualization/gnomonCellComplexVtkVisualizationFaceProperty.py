import os
import logging

import vtk
from vtk import vtkTextActor

from dtkcore import d_bool, d_int, d_inliststring, d_real
import gnomon.core
# import gnomonwidgets
import gnomon.visualization
from gnomon.visualization import gnomonAbstractCellComplexVtkVisualization

from gnomon.utils import load_plugin_group, visualizationPlugin
from cellcomplex.property_topomesh.visualization.vtk_actor_topomesh import VtkActorTopomesh
# from gnomonplugins.visualization.utils.vtk_tools import property_topomesh_to_vtk_polydata, vtk_actor, vertex_scalar_property_polydata, \
#                             edge_scalar_property_polydata, vertex_tensor_property_polydata, face_scalar_property_polydata, \
#                             vtk_combine_polydatas, vtk_scalar_bar_widget, vtk_axes_actor

from gnomon_package_tissuemesh.utils.vtk_actor_topomesh import VtkRender

from .gnomonInteractorStyleCellComplexFaceProperty import gnomonInteractorStyleCellComplexFaceProperty


@visualizationPlugin(version="1.0.0", coreversion="1.0.0", name="Face Properties")
class gnomonCellComplexVtkVisualizationFaceProperty(gnomonAbstractCellComplexVtkVisualization):
    def __init__(self):
        super().__init__()

        self._parameters = {}
        self._parameters['dimension'] = d_int('Dimension', 3, 0, 3, "Dimension of cellcomplex elements to display")
        self._parameters['property_name'] = d_inliststring('Property', "", [""], "Cell property to display")
        self._parameters['tensor_glyph'] = d_inliststring('Tensor glyph', "crosshair", ["crosshair", "ellipsoid"], "Glyph type to display tensor properties")
        self._parameters['vector_glyph'] = d_inliststring('Vector glyph', "arrow", ["arrow", "line"], "Glyph type to display vector properties")
        self._parameters['point_radius'] = d_real("Poinnt radius", 1, 0, 100, 1, "Scale of property to display")
        self._parameters['colormap'] = gnomon.visualization.ParameterColorMap('Colormap', 'glasbey', "Colormap to apply to the property values")
        self._parameters['axes'] = d_bool('Display axes', False, "Display axes")
        self._parameters['opacity'] = d_real('Opacity', 1, 0, 1, 2, "Opacity of the rendered 3D objects")

        self.cellcomplex_series = None
        self.cellcomplex = None
        self.topomesh = None

        # self.mesh_actor = vtk.vtkActor()
        # self.property_actor = vtk.vtkActor()

        self.mesh_actor = None
        self.property_actor = None
        self.vtkrender = VtkRender(create_renderer=False)

        self.assembly = None

        self.renderer3D = None

        self.interactor = None
        self.scalar_bar_widget = None
        self.axes = None

        self.flag_axes = False
        self.flag_scalar_bar = False

        self.style = gnomonInteractorStyleCellComplexFaceProperty()
        self.style.degree = 2
        self.style.setVisualization(self)

    def __del__(self):
        if self.mesh_actor is not None:
            del self.mesh_actor
            self.mesh_actor = None
        if self.property_actor is not None:
            del self.property_actor
            self.property_actor = None
        self.assembly = None
        del self.vtkrender
        self.view().setInteractorStyle(None)
        del self.style

    def clear(self):
        if self.vtkView():
            self.renderer3D = self.vtkView().renderer3D()
            if self.assembly is not None:
                self.renderer3D.RemoveActor(self.assembly)

    def fill(self):
        if self.vtkView():
            self.renderer3D = self.vtkView().renderer3D()
            if self.assembly is not None:
                self.renderer3D.AddActor(self.assembly)

    def setVisible(self, visible):
        if self.mesh_actor is not None:
            self.mesh_actor.SetVisibility(visible)
        if self.property_actor is not None:
            self.property_actor.SetVisibility(visible)
        if self.assembly is not None:
            self.assembly.SetVisibility(visible)

    def setCellComplex(self, cellcomplex_series):
        print(cellcomplex_series)
        print(dir(cellcomplex_series))
        self.cellcomplex_series = cellcomplex_series
        self.cellcomplex = self.cellcomplex_series.current().asCellComplex()
        if hasattr(self.cellcomplex.data(), "_topomesh"):
            self.topomesh = self.cellcomplex.data()._topomesh
        else:
            self.topomesh = gnomonCellComplex_to_topomesh(self.cellcomplex)
        property_name = self['property_name']
        prop = [""] + list(self.topomesh.wisp_properties(2).keys())
        self._parameters['property_name'].setValues(prop)
        self._parameters['property_name'].setValue(property_name if property_name in prop else "")

        self.style.topomesh = self.topomesh
        self.style.view = self.view()

    def interactorStyle(self):
        return self.style

    def imageRendering(self):
        if self.assembly is not None:
            self.updateOffscreenRenderer(*self.mesh_actor.actor.GetMapper().GetInput().GetBounds())
            self.offscreenRenderer().AddActor(self.assembly)
        return self.offscreenImageRendering()

    def update(self):
        self.style.property_name = self['property_name']
        self.renderer3D = self.vtkView().renderer3D()
        self.interactor = self.vtkView().interactor()
        # self.style.SetDefaultRenderer(self.renderer3D)
        # self.interactor.SetInteractorStyle(self.style)

        self.vtkrender.set_view(self.renderer3D, self.interactor)

        # self.mesh_polydata = property_topomesh_to_vtk_polydata(self.topomesh, degree=self['dimension'])
        # self.mesh_actor = vtk_actor(self.mesh_polydata, self.mesh_actor, colormap='Greys_r', value_range=(0, 0), opacity=self['mesh_opacity'])
        #
        # self.property_polydata = property_topomesh_to_vtk_polydata(self.topomesh, degree=2, property_name=self['property_name'], tensor_glyph=self["tensor_glyph"], vector_glyph=self['vector_glyph'], point_radius=self["point_radius"])
        # self.property_actor = vtk_actor(self.property_polydata, self.property_actor, colormap=self._parameters['colormap'].name())

        if self.mesh_actor is None:
            self.mesh_actor = VtkActorTopomesh()
        self.mesh_actor.set_topomesh(self.topomesh, self['dimension'])
        self.mesh_actor.glyph_scale = self["point_radius"]
        self.mesh_actor.update(opacity=self['mesh_opacity'], colormap='Greys_r')

        if self.property_actor is None:
            self.property_actor = VtkActorTopomesh()
        self.property_actor.set_topomesh(self.topomesh, 2, self['property_name'])
        self.property_actor.tensor_glyph = self["tensor_glyph"]
        self.property_actor.vector_glyph = self['vector_glyph']
        self.property_actor.glyph_scale = self["point_radius"]
        self.property_actor.update(colormap=self._parameters['colormap'].name())

        self.vtkrender.set_actors(self.mesh_actor, self.property_actor)

        if self.assembly is None:
            self.assembly = self.vtkrender.get_actors()
            self.renderer3D.AddActor(self.assembly)

        if self['axes'] and not self.flag_axes:
            self.vtkrender.set_axes()
            self.vtkrender.axes_widget.SetEnabled(1)
            self.vtkrender.axes_widget.InteractiveOn()
            self.flag_axes = True
        elif not self['axes'] and self.flag_axes:
            self.vtkrender.axes_widget.SetEnabled(0)
            self.flag_axes = False

        # self.vtkView().resetCamera()
        self.render()

    def render(self):
        self.view().render()

    def onTimeChanged(self, value):
        if value in self.cellcomplex_series.times():
            self.cellcomplex = self.cellcomplex_series.at(value).asCellComplex()
            self.topomesh = self.cellcomplex.data()._topomesh
            self.update()
        self.render()

    def on3D(self):
        pass

    def on2D(self):
        pass

    def onXY(self):
        pass

    def onYZ(self):
        pass

    def onXZ(self):
        pass

    def onSliceOrientationChanged(self):
        pass

    def onSliceChanged(self):
        pass
