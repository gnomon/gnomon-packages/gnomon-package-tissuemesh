import os
import logging

import vtk

from dtkcore import d_inliststring, d_real, d_range_real, array_real_2
import gnomon.core
# import gnomonwidgets
import gnomon.visualization
from gnomon.visualization import gnomonAbstractCellComplexVtkVisualization

from gnomon.utils import visualizationPlugin
from gnomon.utils.decorators import cellComplexInput

from cellcomplex.utils import array_dict
from cellcomplex.property_topomesh.visualization.vtk_actor_topomesh import VtkActorTopomesh

from visu_core.vtk.utils.polydata_tools import vtk_slice_polydata, vtk_glyph_polydata

from visu_core.matplotlib import glasbey
from visu_core.matplotlib.colormap import plain_colormap

from .gnomonInteractorStyleCellComplexFaceProperty import gnomonInteractorStyleCellComplexFaceProperty

orientation_axes = {0:'x', 1:'y', 2:'z'}

@visualizationPlugin(version="1.0.0", coreversion="1.0.0", name="Cell Properties")
@cellComplexInput('topomesh', data_plugin="gnomonCellComplexDataPropertyTopomesh")
class gnomonCellComplexVtkVisualizationCellProperty(gnomonAbstractCellComplexVtkVisualization):

    def __init__(self):
        super().__init__()

        self._parameters = {}
        self._parameters['property_name'] = d_inliststring('Property', "", [""], "Cell property to display")
        self._parameters['value_range'] = d_range_real('Value range', array_real_2([0., 1.]), 0., 1., "Value range for color adjustment")
        self._parameters['tensor_glyph'] = d_inliststring('Tensor glyph', "crosshair", ["crosshair", "ellipsoid"], "Glyph type to display tensor properties")
        self._parameters['vector_glyph'] = d_inliststring('Vector glyph', "arrow", ["arrow", "line"], "Glyph type to display vector properties")
        self._parameters['scale_factor'] = d_real('Scale factor', 0.99, 0, 100, 2, "Scale of the property to display")
        self._parameters['line_glyph'] = d_inliststring('Line glyph', "tube", ["tube", "line"], "Glyph type to display edges")
        self._parameters['slice_thickness'] = d_real('Slice thickness', 2, 0, 5, 2, "Thickness of the 2D slice to display")
        self._parameters['colormap'] = gnomon.visualization.ParameterColorMap('Colormap', 'glasbey', "Colormap to apply to the property values")
        self._parameters['opacity'] = d_real('Opacity', 1, 0, 1, 2, "Opacity of the rendered 3D objects")

        self.topomesh = {}
        self.current_time = 0

        self.cell_actor3D = None
        self.cell_polydata3D = None
        self.cell_actor2D = None
        self.cell_polydata2D = None

        self.edge_actor3D = None
        self.edge_polydata3D = None
        self.edge_actor2D = None
        self.edge_polydata2D = None

        self.cell_edge_actor3D = None
        self.cell_edge_polydata3D = None
        self.cell_edge_actor2D = None
        self.cell_edge_polydata2D = None

        self.vertex_actor3D = None
        self.vertex_polydata3D = None
        self.vertex_actor2D = None
        self.vertex_polydata2D = None
        
        self.slice_orientation = 2
        self.slice_position = 0

        self.is2D = False
        # self.vtkrender = VtkRender(create_renderer=False)

        self.assembly3D = None
        self.assembly2D = None

        self.renderer3D = None
        self.renderer2D = None
        self.interactor = None

        #self.style = gnomonInteractorStyleCellComplexFaceProperty()
        #self.style.degree = 3
        #self.style.setVisualization(self)

    def __del__(self):
        if self.assembly3D is not None:
            del self.cell_actor3D
            del self.edge_actor3D
            self.assembly3D = None
        if self.assembly2D is not None:
            del self.cell_actor2D
            del self.edge_actor2D
            self.assembly2D = None
        #self.view().setInteractorStyle(None)
        #del self.style
        #self.style = None

    def clear(self):
        if self.vtkView():
            self.renderer3D = self.vtkView().renderer3D()
            self.renderer2D = self.vtkView().renderer2D()
            if self.assembly3D is not None:
                self.renderer3D.RemoveActor(self.assembly3D)
            if self.assembly2D is not None:
                self.renderer2D.RemoveActor(self.assembly2D)

    def fill(self):
        if self.vtkView():
            self.renderer3D = self.vtkView().renderer3D()
            self.renderer2D = self.vtkView().renderer2D()
            if self.assembly3D is not None:
                self.renderer3D.AddActor(self.assembly3D)
            if self.assembly2D is not None:
                self.renderer2D.AddActor(self.assembly2D)

    def setVisible(self, visible):
        if self.assembly3D is not None:
            self.assembly3D.SetVisibility(visible)
        if self.cell_actor3D is not None:
            self.cell_actor3D.SetVisibility(visible)
        if self.edge_actor3D is not None:
            self.edge_actor3D.SetVisibility(visible)
        if self.assembly2D is not None:
            self.assembly2D.SetVisibility(visible)
        if self.cell_actor2D is not None:
            self.cell_actor2D.SetVisibility(visible)
        if self.edge_actor2D is not None:
            self.edge_actor2D.SetVisibility(visible)

    def refreshParameters(self):
        if self.current_time in self.topomesh.keys():
            topomesh = self.topomesh[self.current_time]
            property_name = self['property_name']
            prop = [""]+list(topomesh.wisp_properties(3).keys())
            self._parameters['property_name'].setValues(prop)
            self._parameters['property_name'].setValue(property_name if property_name in prop else "")

            #self.style.topomesh = topomesh
            #self.style.view = self.view()

    def update_value_range(self):
        print("Update value range!")
        if self.current_time in self.topomesh.keys():
            topomesh = self.topomesh[self.current_time]
            property_name = self['property_name']

            if topomesh.has_wisp_property(property_name, 3, is_computed=True):
                cell_property = topomesh.wisp_property(property_name, 3)
            else:
                cell_property = array_dict({c:c for c in topomesh.wisps(3)})

            self['value_range'].vtkView().setBounds(array_real_2([cell_property.values().min(),
                                                        cell_property.values().max()]))
            self['value_range'].setValue(array_real_2([cell_property.values().min(),
                                                       cell_property.values().max()]))

    #def interactorStyle(self):
        #return self.style

    def imageRendering(self):
        if self.cell_actor3D is not None:
            self.updateOffscreenRenderer(*self.cell_actor3D.GetMapper().GetInput().GetBounds())
            self.offscreenRenderer().AddActor(self.cell_actor3D)
            self.offscreenRenderer().AddActor(self.edge_actor3D)
        return self.offscreenImageRendering()

    def update(self):
        if self.current_time in self.topomesh.keys():
            topomesh = self.topomesh[self.current_time]

            #self.style.topomesh = topomesh
            #self.style.property_name = self['property_name']

            self.renderer3D = self.vtkView().renderer3D()
            self.renderer2D = self.vtkView().renderer2D()

            if self.cell_actor3D is None:
                self.cell_actor3D = VtkActorTopomesh()

            self.cell_actor3D.set_topomesh(topomesh, 3, self['property_name'])
            self.cell_actor3D.scale_factor = self["scale_factor"]
            self.cell_actor3D.glyph_scale = self["scale_factor"]
            self.cell_actor3D.tensor_glyph = self["tensor_glyph"]
            self.cell_actor3D.vector_glyph = self['vector_glyph']
            colormap = self._parameters['colormap'].name()
            self.cell_actor3D.update(colormap=colormap,
                                     value_range=(0, 255) if colormap=='glasbey' else tuple(self['value_range']),
                                     opacity=self['opacity'])

            self.cell_polydata3D = self.cell_actor3D.polydata

            if self.cell_edge_actor3D is None:
                self.cell_edge_actor3D = VtkActorTopomesh()

            self.cell_edge_actor3D.set_topomesh(topomesh, 1)
            self.cell_edge_actor3D.line_glyph = self['line_glyph']
            self.cell_edge_actor3D.glyph_scale = 0.1*self["scale_factor"]
            self.cell_edge_actor3D.cell_cell_edges = True
            self.cell_edge_actor3D.update(colormap=plain_colormap('k'),
                                     opacity=0.99*self['opacity'], )

            self.cell_edge_polydata3D = self.cell_edge_actor3D.polydata

            if self.edge_actor3D is None:
                self.edge_actor3D = VtkActorTopomesh()

            self.edge_actor3D.set_topomesh(topomesh, 1)
            self.edge_actor3D.line_glyph = self['line_glyph']
            self.edge_actor3D.glyph_scale = 0.02*self["scale_factor"]
            self.edge_actor3D.update(colormap=plain_colormap('k'),
                                     opacity=0.99*self['opacity'], )

            self.edge_polydata3D = self.edge_actor3D.polydata
            

            if self.vertex_actor3D is None:
                self.vertex_actor3D = VtkActorTopomesh()

            self.vertex_actor3D.point_glyph = 'sphere'
            self.vertex_actor3D.glyph_scale = 0.25*self["scale_factor"]
            if topomesh.has_wisp_property("dimension", 0, is_computed=True):
                self.vertex_actor3D.set_topomesh(topomesh, 0, property_name='dimension')
                self.vertex_actor3D.update(colormap='jet',
                                           value_range=(-1, 3),
                                           opacity=self['opacity'], )
            else:
                self.vertex_actor3D.set_topomesh(topomesh, 0)
                self.vertex_actor3D.update(colormap=plain_colormap('k'),
                                           opacity=self['opacity'], )

            self.vertex_polydata3D = self.vertex_actor3D.polydata

            if self.assembly3D is None:
                bounds = self.cell_polydata3D.GetBounds()
                self.vtkView().setBounds(*bounds)

                self.assembly3D = vtk.vtkAssembly()
                self.assembly3D.AddPart(self.cell_actor3D)
                #self.assembly3D.AddPart(self.cell_edge_actor3D)
                #self.assembly3D.AddPart(self.edge_actor3D)
                #self.assembly3D.AddPart(self.vertex_actor3D)
                self.renderer3D.AddActor(self.assembly3D)

            if self.cell_actor2D is None:
                self.cell_actor2D = VtkActorTopomesh()

            self.cell_polydata2D = vtk.vtkPolyData()
            self.cell_actor2D.polydata = self.cell_polydata2D
            self.cell_actor2D.display_polydata = None
            self.cell_actor2D.update_actor(colormap=colormap,
                                           value_range=(0, 255) if colormap=='glasbey' else tuple(self['value_range']),
                                           opacity=self['opacity'])
            if self.edge_actor2D is None:
                self.edge_actor2D = VtkActorTopomesh()

            self.edge_polydata2D = vtk.vtkPolyData()
            self.edge_actor2D.polydata = self.edge_polydata2D
            self.edge_actor2D.display_polydata = None
            self.edge_actor2D.update_actor(colormap=plain_colormap('w'),
                                           opacity=self['opacity'])

            if self.assembly2D is None:
                self.assembly2D = vtk.vtkAssembly()
                self.assembly2D.AddPart(self.cell_actor2D)
                self.assembly2D.AddPart(self.edge_actor2D)
                self.renderer2D.AddActor(self.assembly2D)

            self.onSliceChanged(self.slice_position)
            self.vtkView().resetCamera()
            self.render()

    def render(self):
        self.view().render()

    def onTimeChanged(self, value):
        if value in self.topomesh.keys():
            self.current_time = value

            self.refreshParameters()
            self.update()
        self.render()

    def on3D(self):
        self.is2D = False
        self.interactor = self.vtkView().interactor()
        #self.style.mode = "3D"
        self.render()

    def on2D(self):
        self.is2D = True
        self.interactor = self.vtkView().interactor()
        #self.style.mode = "2D"
        self.render()

    def onXY(self):
        self.render()

    def onYZ(self):
        self.render()

    def onXZ(self):
        self.render()

    def onSliceOrientationChanged(self, value):
        self.slice_orientation = value

    def onSliceChanged(self, value):
        self.slice_position = value
        if self.cell_polydata3D is not None:
            self.cell_polydata2D = vtk_slice_polydata(self.cell_polydata3D,
                                                       axis=orientation_axes[self.slice_orientation],
                                                       position=self.slice_position,
                                                       width=self['slice_thickness'])
            self.cell_actor2D.polydata = self.cell_polydata2D
            if self.cell_polydata2D.GetPointData().GetNumberOfArrays() > 0:
                self.cell_actor2D.display_polydata = vtk_glyph_polydata(self.cell_actor2D.polydata,
                                                                         vector_glyph=self['vector_glyph'],
                                                                         tensor_glyph=self['tensor_glyph'],
                                                                         glyph_scale=self['scale_factor'])
                self.cell_actor2D.scalar_mode = 'point_data'
            else:
                self.cell_actor2D.display_polydata = None
                self.cell_actor2D.scalar_mode = 'cell_data'

            colormap = self._parameters['colormap'].name()
            self.cell_actor2D.update_actor(opacity=self['opacity'], colormap=colormap, value_range=(0, 255) if colormap == 'glasbey' else None)

        if self.edge_polydata3D is not None:
            self.edge_polydata2D = vtk_slice_polydata(self.edge_polydata3D,
                                                       axis=orientation_axes[self.slice_orientation],
                                                       position=self.slice_position,
                                                       width=self['slice_thickness'])
            self.edge_actor2D.polydata = self.edge_polydata2D
            self.edge_actor2D.display_polydata = None

            self.edge_actor2D.update_actor(opacity=self['opacity'], colormap=plain_colormap('w'))

        self.render()

