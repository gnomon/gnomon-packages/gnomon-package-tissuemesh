import os
import logging

import numpy as np
import vtk
import pyvista as pv

from dtkcore import d_real, d_bool, d_inliststring, d_range_real, array_real_2
import gnomon.visualization
from gnomon.visualization import gnomonAbstractMeshVtkVisualization, ParameterColorMap

from gnomon.utils import visualizationPlugin
from gnomon.utils.decorators import meshInput

from cellcomplex.property_topomesh.visualization.pyvista import topomesh_unstructured_grid

from visu_core.matplotlib.colormap import plain_colormap

color_list = [
    "black", "grey", "lightgrey", "white", "darkred", "red", "darkorange",
    "darkgoldenrod", "gold", "yellow", "yellowgreen", "green", "limegreen",
    "mediumseagreen", "darkturquoise", "deepskyblue", "cornflowerblue", "navy",
    "blue", "mediumpurple", "darkviolet", "magenta", "mediumvioletred"
]

@visualizationPlugin(version="1.0.0", coreversion="1.0.0", name="Mesh Unstructured Grid")
@meshInput('topomesh', data_plugin="gnomonMeshDataPropertyTopomesh")
class meshVisualizationPyvista(gnomonAbstractMeshVtkVisualization):
    def __init__(self):
        super().__init__()

        self._parameters = {}
        self._parameters['position'] = d_inliststring("Position", "", [""], "Property to use as spatial reference")

        self._parameters['sphere_vertices'] = d_bool('Sphere vertices', True, "Whether to display vertices as spheres or points")
        self._parameters['point_size'] = d_real('Point size', 1, 0, 100, 2, "Size used to display vertex spheres")
        self._parameters['tube_edges'] = d_bool('Tube edges', True, "Whether to display edges as tubes or lines")
        self._parameters['line_width'] = d_real('Line width', 3, 1, 20, 1, "Size used to display edge lines/tubes")
        self._parameters['opacity'] = d_real('Opacity', 1, 0, 2, "Opacity of the displayed surface")

        self._parameters['scalars'] = d_inliststring("Scalar attribute", "", [""], "Attribute to display on the mesh")
        self._parameters["value_range"] = d_range_real("Value range", array_real_2([0., 1.]), 0., 1., "Value range for color adjustment")
        self._parameters["colormap"] = ParameterColorMap("Colormap", "temperature", "Colormap to apply to the mesh attribute")

        self._parameter_groups = {}
        for parameter_name in ['colormap', 'value_range']:
            self._parameter_groups[parameter_name] = 'rendering'
        for parameter_name in ['sphere_vertices', 'point_size', 'tube_edges', 'line_width']:
            self._parameter_groups[parameter_name] = 'glyphs'

        self.connectParameter("scalars")

        self.topomesh = {}
        self.current_time = 0

        self.grid = None

        self.assembly = None

        self.renderer3D = None

    def __del__(self):
        if self.assembly is not None:
            del self.assembly
            self.assembly = None

    def clear(self):
        if self.vtkView():
            self.renderer3D = self.vtkView().renderer3D()
            if self.assembly is not None:
                self.renderer3D.RemoveActor(self.assembly)

    def fill(self):
        if self.vtkView():
            self.renderer3D = self.vtkView().renderer3D()
            if self.assembly is not None:
                self.renderer3D.AddActor(self.assembly)

    def setVisible(self, visible):
        if self.assembly is not None:
            self.assembly.SetVisibility(visible)

    def refreshParameters(self):
        print("Refresh parameters mesh visu")
        if len(self.topomesh)>0:
            self.current_time = list(self.topomesh.keys())[0]

            topomesh = self.topomesh[self.current_time]
            self._parameters['sphere_vertices'].setValue(topomesh.nb_wisps(0) <= 10000)
            self._parameters['tube_edges'].setValue(topomesh.nb_wisps(1) <= 5000)

            position_properties = [p for p in topomesh.wisp_property_names(0) if p.endswith("barycenter")]
            position_properties = [p for p in position_properties if topomesh.has_wisp_property(p, 0, is_computed=True)]
            position_properties = [p for p in position_properties if topomesh.wisp_property(p, 0).values().dtype != np.dtype('O')]
            position_properties = [p for p in position_properties if topomesh.wisp_property(p, 0).values().ndim == 2]

            self._parameters['position'].setValues(position_properties)
            if len(position_properties) > 0:
                if 'barycenter' in position_properties:
                    self._parameters['position'].setValue("barycenter")
                else:
                    self._parameters['position'].setValue(position_properties[0])

            self._update_attribute_list()
            self._update_value_range()

    def imageRendering(self):
        if self.grid:
            self.updateOffscreenRenderer(*self.grid.GetBounds())
        if self.assembly:
            self.offscreenRenderer().AddActor(self.assembly)
        return self.offscreenImageRendering()

    def onParameterChanged(self, parameter_name=None):
        if parameter_name == "scalars":
            self._update_value_range()

    def _update_attribute_list(self):
        if len(self.topomesh)>0:
            topomesh = self.topomesh[self.current_time]

            attribute_name = self['scalars']
            property_names = list(topomesh.wisp_property_names(0))
            property_names = [p for p in property_names if topomesh.has_wisp_property(p, 0, is_computed=True)]
            property_names = [p for p in property_names if topomesh.wisp_property(p, 0).values().ndim == 1]
            property_names = [p for p in property_names if topomesh.wisp_property(p, 0).values().dtype != np.dtype('O')]

            prop = [""] + property_names
            self._parameters['scalars'].setValues(prop)
            self._parameters['scalars'].setValue(attribute_name if attribute_name in prop else "")

    def _update_value_range(self):
        if len(self.topomesh)>0:
            topomesh = self.topomesh[self.current_time]

            attribute_name = self['scalars']
            degree = 0

            if attribute_name in topomesh.wisp_property_names(degree):
                property_data = topomesh.wisp_property(attribute_name, degree).values(list(topomesh.wisps(degree)))
            else:
                property_data = np.array(list(topomesh.wisps(degree)))

            min_p = float(np.around(np.nanmin(property_data), decimals=3))
            max_p = float(np.around(np.nanmax(property_data), decimals=3))

            self._parameters['value_range'].setMin(np.around(min_p - (max_p - min_p)/2, decimals=3))
            self._parameters['value_range'].setMax(np.around(max_p + (max_p - min_p)/2, decimals=3))
            self._parameters['value_range'].setValueMin(min_p)
            self._parameters['value_range'].setValueMax(max_p)

    def update(self):
        self.renderer3D = self.vtkView().renderer3D()

        if self.current_time in self.topomesh.keys():
            topomesh = self.topomesh[self.current_time]

            self.grid = topomesh_unstructured_grid(
                topomesh, save_cells=False, position_property=self['position']
            )
            if self['sphere_vertices']:
                vertex_grid = self.grid.glyph(
                    geom=pv.Sphere(radius=self['point_size']/2),
                    scale=False,
                    orient=False
                )

            colormap = self._parameters['colormap'].name()
            value_range = self['value_range']
            if colormap == "glasbey":
                value_range = (0, 255)

            plotter = pv.Plotter(off_screen=True)
            if self['scalars'] != "":
                plotter.add_mesh(
                    self.grid, scalars=self['scalars'],
                    cmap=colormap, clim=value_range,
                    interpolate_before_map=False,
                    render_lines_as_tubes=self['tube_edges'],
                    line_width=self['line_width'],
                    show_scalar_bar=False,
                    opacity=self['opacity']
                )
                if self['sphere_vertices']:
                    plotter.add_mesh(
                        vertex_grid, scalars=self['scalars'],
                        cmap=colormap, clim=value_range,
                        show_scalar_bar=False,
                        opacity=self['opacity']
                    )
            else:
                plotter.add_mesh(
                    self.grid, color='w',
                    render_lines_as_tubes=self['tube_edges'],
                    line_width=self['line_width'],
                    opacity=self['opacity']
                )
                if self['sphere_vertices']:
                    plotter.add_mesh(
                        vertex_grid, color='w',
                        opacity=self['opacity']
                    )

            ren = plotter.renderer

            if self.assembly:
                self.renderer3D.RemoveActor(self.assembly)
            self.assembly = vtk.vtkAssembly()
            for key, actor in ren.actors.items():
                self.assembly.AddPart(actor)
            self.renderer3D.AddActor(self.assembly)

            self.vtkView().resetCamera()
            self.render()

    def render(self):
        self.view().render()

    def onTimeChanged(self, value):
        if value in self.topomesh.keys():
            self.current_time = value
            self.update()
        self.render()

    def on3D(self):
        pass

    def on2D(self):
        pass

    def onXY(self):
        pass

    def onYZ(self):
        pass

    def onXZ(self):
        pass

    def onSliceOrientationChanged(self):
        pass

    def onSliceChanged(self):
        pass
