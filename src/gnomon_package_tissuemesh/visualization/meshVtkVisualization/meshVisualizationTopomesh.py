import os
import logging

import numpy as np
import vtk

from dtkcore import d_real, d_bool, d_inliststring, d_range_real, array_real_2
import gnomon.visualization
from gnomon.visualization import gnomonAbstractMeshVtkVisualization, ParameterColorMap

from gnomon.utils import visualizationPlugin
from gnomon.utils.decorators import meshInput

from cellcomplex.property_topomesh.visualization.vtk_actor_topomesh import VtkActorTopomesh

from visu_core.matplotlib.colormap import plain_colormap

color_list = [
    "black", "grey", "lightgrey", "white", "darkred", "red", "darkorange",
    "darkgoldenrod", "gold", "yellow", "yellowgreen", "green", "limegreen",
    "mediumseagreen", "darkturquoise", "deepskyblue", "cornflowerblue", "navy",
    "blue", "mediumpurple", "darkviolet", "magenta", "mediumvioletred"
]

@visualizationPlugin(version="1.0.0", coreversion="1.0.0", name="Mesh Edges & Vertices")
@meshInput('topomesh', data_plugin="gnomonMeshDataPropertyTopomesh")
class meshVisualizationTopomesh(gnomonAbstractMeshVtkVisualization):
    def __init__(self):
        super().__init__()

        self._parameters = {}
        self._parameters['scale_factor'] = d_real('Scale factor', 1, 0, 100, 2, "Scale factor for edge and vertex diameters")
        self._parameters['tube_edges'] = d_bool('Tube edges', True, "Whether to display edges as tubes or lines")
        self._parameters['sphere_vertices'] = d_bool('Sphere vertices', True, "Whether to display vetices as spheres or points")

        self._parameters['attribute_elements'] = d_inliststring("Attribute Elements", "vertices", ["vertices", "edges", "faces"], "Whether to display attributes on vertices, edges or faces")
        self._parameters['attribute'] = d_inliststring("Attribute", "", [""], "Attribute to display on the mesh")

        self._parameters["value_range"] = d_range_real("Value range", array_real_2([0., 1.]), 0., 1., "Value range for color adjustment")
        self._parameters["colormap"] = ParameterColorMap("Colormap", "temperature", "Colormap to apply to the mesh attribute")

        self._parameters["face_color"] = d_inliststring("Face Color", "white", color_list)
        self._parameters["edge_color"] = d_inliststring("Edge Color", "navy", color_list)
        self._parameters["vertex_color"] = d_inliststring("Vertex Color", "darkred", color_list)

        self._parameter_groups = {}
        for parameter_name in ['colormap', 'value_range']:
            self._parameter_groups[parameter_name] = 'rendering'
        for parameter_name in ['scale_factor', 'sphere_vertices', 'tube_edges']:
            self._parameter_groups[parameter_name] = 'glyphs'
        for parameter_name in ['face_color', 'edge_color', 'vertex_color']:
            self._parameter_groups[parameter_name] = 'colors'

        self.connectParameter("attribute_elements")
        self.connectParameter("attribute")

        self.topomesh = {}
        self.current_time = 0

        self.face_actor = None
        self.edge_actor = None
        self.vertex_actor = None

        self.assembly = None

        self.renderer3D = None

    def __del__(self):
        if self.face_actor is not None:
            del self.face_actor
            self.face_actor = None
        if self.edge_actor is not None:
            del self.edge_actor
            self.edge_actor = None
        if self.vertex_actor is not None:
            del self.vertex_actor
            self.vertex_actor = None
        self.assembly = None

    def clear(self):
        if self.vtkView():
            self.renderer3D = self.vtkView().renderer3D()
            if self.assembly is not None:
                self.renderer3D.RemoveActor(self.assembly)

    def fill(self):
        if self.vtkView():
            self.renderer3D = self.vtkView().renderer3D()
            if self.assembly is not None:
                self.renderer3D.AddActor(self.assembly)

    def setVisible(self, visible):
        if self.face_actor is not None:
            self.face_actor.SetVisibility(visible)
        if self.edge_actor is not None:
            self.edge_actor.SetVisibility(visible)
        if self.vertex_actor is not None:
            self.vertex_actor.SetVisibility(visible)
        if self.assembly is not None:
            self.assembly.SetVisibility(visible)

    def refreshParameters(self):
        print("Refresh parameters mesh visu")
        if len(self.topomesh)>0:
            self.current_time = list(self.topomesh.keys())[0]

            topomesh = self.topomesh[self.current_time]
            self._parameters['sphere_vertices'].setValue(topomesh.nb_wisps(0) <= 10000)
            self._parameters['tube_edges'].setValue(topomesh.nb_wisps(1) <= 5000)

            self._update_attribute_list()
            self._update_value_range()

    def imageRendering(self):
        if self.assembly is not None:
            self.updateOffscreenRenderer(*self.face_actor.GetMapper().GetInput().GetBounds())
            self.offscreenRenderer().AddActor(self.assembly)
        return self.offscreenImageRendering()

    def onParameterChanged(self, parameter_name=None):
        if parameter_name == "attribute_elements":
            self._update_attribute_list()
        if parameter_name == "attribute":
            self._update_value_range()

    def _update_attribute_list(self):
        if len(self.topomesh)>0:
            topomesh = self.topomesh[self.current_time]

            attribute_name = self['attribute']
            element_degree = {'vertices': 0, 'edges': 1, 'faces': 2}
            degree = element_degree[self['attribute_elements']]
            property_names = list(topomesh.wisp_property_names(degree))
            property_names = [p for p in property_names if topomesh.has_wisp_property(p, degree, is_computed=True)]
            property_names = [p for p in property_names if topomesh.wisp_property(p, degree).values().ndim == 1]
            property_names = [p for p in property_names if topomesh.wisp_property(p, degree).values().dtype != np.dtype('O')]

            prop = [""] + property_names
            self._parameters['attribute'].setValues(prop)
            self._parameters['attribute'].setValue(attribute_name if attribute_name in prop else "")

    def _update_value_range(self):
        if len(self.topomesh)>0:
            topomesh = self.topomesh[self.current_time]

            attribute_name = self['attribute']
            element_degree = {'vertices': 0, 'edges': 1, 'faces': 2}
            degree = element_degree[self['attribute_elements']]

            if attribute_name in topomesh.wisp_property_names(degree):
                property_data = topomesh.wisp_property(attribute_name, degree).values(list(topomesh.wisps(degree)))
            else:
                property_data = np.array(list(topomesh.wisps(degree)))

            min_p = float(np.around(np.nanmin(property_data), decimals=3))
            max_p = float(np.around(np.nanmax(property_data), decimals=3))

            self._parameters['value_range'].setMin(np.around(min_p - (max_p - min_p)/2, decimals=3))
            self._parameters['value_range'].setMax(np.around(max_p + (max_p - min_p)/2, decimals=3))
            self._parameters['value_range'].setValueMin(min_p)
            self._parameters['value_range'].setValueMax(max_p)

    def update(self):
        self.renderer3D = self.vtkView().renderer3D()

        if self.current_time in self.topomesh.keys():
            topomesh = self.topomesh[self.current_time]

            element_degree = {'vertices': 0, 'edges': 1, 'faces': 2}
            degree = element_degree[self['attribute_elements']]
            colormap = self._parameters['colormap'].name()
            value_range = self['value_range']
            if colormap == "glasbey":
                value_range = (0, 255)

            if self.face_actor is None:
                self.face_actor = VtkActorTopomesh()
            self.face_actor.set_topomesh(topomesh, 2)
            if self["attribute"] == "" or degree == 1:
                self.face_actor.update(colormap=plain_colormap(self['face_color']))
            else:
                self.face_actor._property_name = self["attribute"]
                self.face_actor._property_degree = degree
                self.face_actor.update(colormap=colormap, value_range=value_range)

            if self.edge_actor is None:
                self.edge_actor = VtkActorTopomesh()
            self.edge_actor.set_topomesh(topomesh, 1)
            self.edge_actor.line_glyph = 'tube' if self['tube_edges'] else 'line'
            self.edge_actor.glyph_scale = 0.1*self['scale_factor']
            if self["attribute"] == "" or degree != 1:
                self.edge_actor.update(colormap=plain_colormap(self['edge_color']))
            else:
                self.edge_actor._property_name = self["attribute"]
                self.edge_actor._property_degree = 1
                print(self.edge_actor.property_name, topomesh.has_wisp_property(self.edge_actor.property_name, 1, is_computed=True))
                self.edge_actor.update(colormap=colormap, value_range=value_range)

            if self.vertex_actor is None:
                self.vertex_actor = VtkActorTopomesh()
            self.vertex_actor.set_topomesh(topomesh, 0)
            self.vertex_actor.point_glyph = 'sphere' if self['sphere_vertices'] else 'point'
            self.vertex_actor.glyph_scale = 0.25*self['scale_factor']
            if degree != 0 or self["attribute"] == "":
                self.vertex_actor.update(colormap=plain_colormap(self['vertex_color']))
            else:
                self.vertex_actor._property_name = self["attribute"]
                self.vertex_actor._property_degree = degree
                self.vertex_actor.update(colormap=colormap, value_range=value_range)

            if self.assembly is None:
                self.assembly = vtk.vtkAssembly()
                self.assembly.AddPart(self.face_actor)
                self.assembly.AddPart(self.edge_actor)
                self.assembly.AddPart(self.vertex_actor)
                self.renderer3D.AddActor(self.assembly)

            self.vtkView().resetCamera()
            self.render()

    def render(self):
        self.view().render()

    def onTimeChanged(self, value):
        if value in self.topomesh.keys():
            self.current_time = value
            self.update()
        self.render()

    def on3D(self):
        pass

    def on2D(self):
        pass

    def onXY(self):
        pass

    def onYZ(self):
        pass

    def onXZ(self):
        pass

    def onSliceOrientationChanged(self):
        pass

    def onSliceChanged(self):
        pass
