import logging
from copy import deepcopy
from typing import Dict

import numpy as np

import gnomon.core
from gnomon.utils import formDataPlugin, serialize
from gnomon.core import gnomonAbstractMeshData, gnomonMesh, gnomonMeshAttribute

from cellcomplex.property_topomesh import PropertyTopomesh
from cellcomplex.property_topomesh.creation import triangle_topomesh
from cellcomplex.property_topomesh.analysis import compute_topomesh_property
from cellcomplex.property_topomesh.analysis import is_triangular


@formDataPlugin(version="1.0.0", coreversion="1.0.0", data_setter="set_property_topomesh", data_getter="get_property_topomesh")
@serialize("_topomesh")
class gnomonMeshDataPropertyTopomesh(gnomonAbstractMeshData):

    def __init__(self, topomesh=None):
        super().__init__()
        if topomesh is None:
            self._topomesh = PropertyTopomesh()
        else:
            self._topomesh = deepcopy(topomesh._topomesh)

    def __del__(self):
        del self._topomesh

    def set_property_topomesh(self, topomesh: PropertyTopomesh):
        assert is_triangular(topomesh)
        self._topomesh = deepcopy(topomesh)

    def get_property_topomesh(self) -> PropertyTopomesh:
        return self._topomesh

    def fromGnomonForm(self, form: gnomonMesh):
        n_points = form.pointsCount()

        positions = {i: form.pointCoordinates(i) for i in range(n_points)}

        face_ids = form.cellsIdx(2)
        face_vertices = [form.cellPointsIdx(f) for f in face_ids]
        triangle_vertices = [v for v in face_vertices if len(v)==3]
        topomesh = triangle_topomesh(triangle_vertices, positions)

        #TODO: set properties from attributes

        self.set_property_topomesh(topomesh)
    
    def clone(self):
        _clone = gnomonMeshDataPropertyTopomesh(self)
        _clone.__disown__()
        return _clone

# Metadata

    def metadata(self):
        metadata = {}
        metadata['Number of faces'] = str(self._topomesh.nb_wisps(2))
        metadata['Number of vertices'] = str(self._topomesh.nb_wisps(0))
        for property_name in self._topomesh.wisp_property_names(2):
            metadata['Face Property '+property_name] = str(self._topomesh.wisp_property(property_name, 2).values().dtype)
        for property_name in self._topomesh.wisp_property_names(0):
            metadata['Vertex Property '+property_name] = str(self._topomesh.wisp_property(property_name, 0).values().dtype)

        return metadata

    def dataName(self):
        return "cellcomplex.PropertyTopomesh"

    def geometricalDimension(self) -> int:
        return 3

    def setGeometricalDimension(self, geo_dim: int) -> None:
        logging.error("The geometrical dimension of a PropertyTopomesh can not be set.")

    def topologicalDimension(self) -> int:
        return self._topomesh.degree()

    def setTopologicalDimension(self, topo_dim: int) -> None:
        logging.error("The topological dimension of a PropertyTopomesh can not be set.")

    def pointCoordinates(self, point_id: int):
        point_coordinates = np.nan * np.ones(3, float)
        if point_id in self._topomesh.wisps(0):
            for k, p in enumerate(self._topomesh.wisp_property('barycenter', 0)[point_id]):
                point_coordinates[k] = p
        return list(point_coordinates)

    def pointsCoordinates(self):
        return [list(self._topomesh.wisp_property('barycenter', 0)[point_id]) for point_id in self._topomesh.wisps(0)]

    def pointsCount(self) -> int:
        return self._topomesh.nb_wisps(0)

    def setPoint(self, point_id: int, points_coordinates) -> None:
        if point_id in self._topomesh.wisps(0):
            self._topomesh.wisp_property('barycenter', 0)[point_id] = np.array(points_coordinates)

    def setPointsCount(self, point_count: int) -> None:
        logging.error("The number of vertices of a PropertyTopomesh can not be set.")

    def setPoints(self, points_coordinates, point_count: int) -> None:
        logging.error("The vertex positions of a PropertyTopomesh can not be set.")

    def cellPointsIdx(self, cell_id: int):
        if cell_id in self._topomesh.wisps(2):
            if not self._topomesh.has_wisp_property('oriented_vertices', 2, is_computed=True):
                compute_topomesh_property(self._topomesh, 'oriented_vertices', 2)
            face_vertices = self._topomesh.wisp_property('oriented_vertices', 2)[cell_id]
            return list(face_vertices)
        else:
            return []

    def cellType(self, cell_id: int):
        if cell_id in self._topomesh.wisps(2):
            return 5 # Triangle
        else:
            return 99 # Unknown

    def cellsCount(self, dimension: int) -> int:
        if dimension >= 2:
            dimension = 2
        if dimension <= self._topomesh.degree():
            return self._topomesh.nb_wisps(dimension)
        else:
            logging.warning(f"There are no elements of dimension {dimension}!")
            return 0

    def cellsIdx(self, dimension: int):
        if dimension >= 2:
            dimension = 2
        if dimension <= self._topomesh.degree():
            return list(self._topomesh.wisps(dimension))
        else:
            logging.warning(f"There are no elements of dimension {dimension}!")
            return []

    def cellsPoints(self, dimension: int):
        if dimension >= 2:
            dimension = 2
        if dimension <= self._topomesh.degree():
            if not self._topomesh.has_wisp_property('oriented_vertices', 2, is_computed=True):
                compute_topomesh_property(self._topomesh, 'oriented_vertices', dimension)
            element_ids = list(self._topomesh.wisps(dimension))
            element_vertices = self._topomesh.wisp_property('oriented_vertices', dimension).values(element_ids)
            element_vertices = np.concatenate(element_vertices)
            return list(element_vertices)
        else:
            logging.warning(f"There are no elements of dimension {dimension}!")
            return []

    def cellsType(self, dimension: int):
        if dimension >= 2:
            dimension = 2
        if dimension == 2:
            return [5 for _ in self._topomesh.wisps(2)] # Triangle
        elif dimension == 1:
            return [3 for _ in self._topomesh.wisps(1)] # Line
        elif dimension == 0:
            return [1 for _ in self._topomesh.wisps(0)] # Vertex
        else:
            return []

    def cellsTopologyLocation(self, dimension):
        if dimension >= 2:
            dimension = 2
        if dimension <= self._topomesh.degree():
            if not self._topomesh.has_wisp_property('oriented_vertices', 2, is_computed=True):
                compute_topomesh_property(self._topomesh, 'oriented_vertices', dimension)
            element_ids = list(self._topomesh.wisps(dimension))
            element_vertices = self._topomesh.wisp_property('oriented_vertices', dimension).values(element_ids)
            return [v[0] for v in element_vertices]
        else:
            logging.warning(f"There are no elements of dimension {dimension}!")
            return []

    def setCellsCount(self, cells_count: int, dimension: int):
        logging.error("The number of cells of a PropertyTopomesh can not be set.")

    def setCellsType(self, cells_type, cells_count: int, dimension: int):
        logging.error("The type of cells of a PropertyTopomesh can not be set.")

    def setCellsPoints(self, cells_points, size_cells_point):
        logging.error("The vertices of cells of a PropertyTopomesh can not be set.")

    def addAttribute(self, attribute: Dict):
        support_dimensions = {1: 0, 2: 2}
        kind_shapes = {1: (-1,), 3: (-1, 3), 9: (-1, 3, 3)}

        dimension = support_dimensions.get(attribute['support'], None)
        if dimension is not None:
            property_name = attribute['name']
            property_shape = kind_shapes.get(attribute['kind'], (-1,))
            property_data = attribute['data'].reshape(property_shape)

            if len(property_data) == self._topomesh.nb_wisps(dimension):
                property_dict = dict(zip(self._topomesh.wisps(dimension), property_data))
                self._topomesh.update_wisp_property(property_name, dimension, property_dict)
            else:
                logging.error(f"The attribute data has len {len(property_data)} while the PropertyTopomesh has {self._topomesh.nb_wisps(dimension)} elements of dimension {dimension}")

    def attribute(self, name: str) -> Dict:
        if name in self._topomesh.wisp_property_names(2):
            dimension = 2
        elif name in self._topomesh.wisp_property_names(0):
            dimension = 0
        else:
            dimension = None

        if dimension is not None:
            if self._topomesh.has_wisp_property(name, dimension, is_computed=True):

                dimension_supports = {0: 1, 2: 2}

                property_dict = self._topomesh.wisp_property(name, dimension)
                property_data = property_dict.values(list(self._topomesh.wisps(dimension)))
                kind = None
                if property_data.ndim == 1:
                    if property_data.dtype != np.dtype("O"):
                        kind = 1
                    else:
                        logging.error(f"{name} ({dimension}): Not the right scalar property type! {property_data.dtype}")
                elif property_data.ndim == 2:
                    if property_data.shape[1] == 3:
                        kind = 3
                    else:
                        logging.error(f"{name} ({dimension}): Not the right vector property shape! {property_data.shape}")
                elif property_data.ndim == 3:
                    if property_data.shape[1] == 3 and property_data.shape[2] == 3:
                        kind = 9
                    else:
                        logging.error(f"{name} ({dimension}): Not the right tensor property shape! {property_data.shape}")

                if kind is not None:
                    attr = {
                        'name': name,
                        'support': dimension_supports[dimension],
                        'kind': kind,
                        'data': property_data.astype(np.float64).ravel()
                    }
                    return attr
                else:
                    return {}
            else:
                return {}
        else:
            return {}

    def attributes(self):
        attr_list = []
        for dimension in [0, 2]:
            for name in self._topomesh.wisp_property_names(dimension):
                if self._topomesh.has_wisp_property(name, dimension, is_computed=True):
                    attr = self.attribute(name)
                    if attr != {}:
                        attr_list += [attr]
        return attr_list

    def attributesCount(self) -> int:
        attr_list = self.attributes()
        return len(attr_list)

    def attributesNames(self):
        attr_names = []
        for dimension in [0, 2]:
            for name in self._topomesh.wisp_property_names(dimension):
                if self._topomesh.has_wisp_property(name, dimension, is_computed=True):
                    attr = self.attribute(name)
                    if attr != {}:
                        attr_names += [name]
        print(attr_names)
        return attr_names
