import gnomon.core
import numpy as np

from gnomon.core import gnomonAbstractCellComplexData, gnomonCellComplex

from cellcomplex.property_topomesh.analysis import compute_topomesh_property, is_triangular
from copy import deepcopy

from cellcomplex.property_topomesh import PropertyTopomesh

from gnomon.utils import formDataPlugin, serialize


@formDataPlugin(version="1.0.0", coreversion="1.0.0", data_setter="set_property_topomesh", data_getter="get_property_topomesh")
@serialize("_topomesh")
class gnomonCellComplexDataPropertyTopomesh(gnomonAbstractCellComplexData):

    def __init__(self, cellcomplex=None):
        super().__init__()
        if cellcomplex is None:
            self._topomesh = PropertyTopomesh()
        else:
            self._topomesh = deepcopy(cellcomplex._topomesh)

    def __del__(self):
        del self._topomesh

    def set_property_topomesh(self, topomesh: PropertyTopomesh):
        self._topomesh = deepcopy(topomesh)

    def get_property_topomesh(self) -> PropertyTopomesh:
        return self._topomesh

    def clone(self):
        _clone = gnomonCellComplexDataPropertyTopomesh(self)
        _clone.__disown__()
        return _clone

    def fromGnomonForm(self, form: gnomonCellComplex):
        topomesh = PropertyTopomesh(form.dimension())
        for dim in range(form.dimension()+1):
            for eid in form.elementIds(dim):
                topomesh.add_wisp(dim, eid)
                if dim > 0:
                    for bid in form.incidentElementIds(dim, eid, dim-1):
                        topomesh.link(dim, eid, bid)

            for property_name in form.elementPropertyNames(dim):
                property_dict = form.elementProperty(dim, property_name)
                topomesh.update_wisp_property(property_name, dim, property_dict)

        if all([form.hasElementProperty(0, "barycenter_"+dim) for dim in 'xyz']):
            vertex_points = []
            for dim in 'xyz':
                vertex_coords = form.elementProperty(0, "barycenter_"+dim)
                vertex_points += [vertex_coords[vid] for vid in topomesh.wisps(0)]
            vertex_positions = dict(zip(topomesh.wisps(0), np.transpose(vertex_points)))
            topomesh.update_wisp_property('barycenter', 0, vertex_positions)
        self.set_property_topomesh(topomesh)

    def metadata(self):
        metadata = {}
        metadata['Number of cells'] = str(self._topomesh.nb_wisps(3))
        metadata['Number of faces'] = str(self._topomesh.nb_wisps(2))
        metadata['Number of vertices'] = str(self._topomesh.nb_wisps(1))
        metadata['Triangulated faces'] = str(is_triangular((self._topomesh)))
        for property_name in self._topomesh.wisp_property_names(3):
            metadata['Property '+property_name] = str(self._topomesh.wisp_property(property_name, 3).values().dtype)

        return metadata

    def dataName(self):
        return "cellcomplex.PropertyTopomesh"

    def setDimension(self, dimension=3):
        self._topomesh._degree = dimension

    def dimension(self):
        return self._topomesh.degree()

    def isValid(self):
        return self._topomesh.is_valid()

# Element concept

    def hasElement(self, dimension, id):
        return self._topomesh.has_wisp(degree=dimension, wid=id)

    def elementIds(self, dimension):
        return list(self._topomesh.wisps(degree=dimension))

    def elementCount(self, dimension):
        return self._topomesh.nb_wisps(degree=dimension)

# Incidence concept

    def incidentElementIds(self, dimension, id, incidenceDimension):
        if incidenceDimension == dimension:
            return [id]
        elif incidenceDimension < dimension:
            return self.borderIds(dimension, id, dimension-incidenceDimension)
        else:
            return self.regionIds(dimension, id, incidenceDimension-dimension)

    def incidentElementCount(self, dimension, id, incidenceDimension):
        return len(self.incidentElementIds(dimension, id, incidenceDimension))

    def borderIds(self, dimension, id, offset=1):
        return list(self._topomesh.borders(degree=dimension, wid=id, offset=offset))

    def borderCount(self, dimension, id):
        return self._topomesh.nb_borders(degree=dimension, wid=id)

    def regionIds(self, dimension, id, offset=1):
        return list(self._topomesh.regions(degree=dimension, wid=id, offset=offset))

    def regionCount(self, dimension, id):
        return self._topomesh.nb_regions(degree=dimension, wid=id)

# Neighborhood concept

    def adjacentElementIds(self, dimension, id, incidenceDimension):
        if incidenceDimension > dimension:
            return self.regionAdjacentElementIds(dimension, id)
        else:
            return self.borderAdjacentElementIds(dimension, id)

    def adjacentElementCount(self, dimension, id, incidenceDimension):
        return len(self.adjacentElementIds(dimension, id, incidenceDimension))

    def borderAdjacentElementIds(self, dimension, id):
        return list(self._topomesh.border_neighbors(degree=dimension, wid=id))

    def borderAdjacentElementCount(self, dimension, id):
        return self._topomesh.nb_border_neighbors(degree=dimension, wid=id)

    def regionAdjacentElementIds(self, dimension, id):
        return list(self._topomesh.region_neighbors(degree=dimension, wid=id))

    def regionAdjacentElementCount(self, dimension, id):
        return self._topomesh.nb_region_neighbors(degree=dimension, wid=id)

# Mutation concept

    def addElement(self, dimension, id=None):
        return self._topomesh.add_wisp(degree=dimension, wid=id)

    def removeElement(self, dimension, id):
        return self._topomesh.remove_wisp(degree=dimension, wid=id)

    def linkElements(self, dimension, id, incidentId):
        return self._topomesh.link(degree=dimension, wid=id, border_id=incidentId)

    def unlinkElements(self, dimension, id, incidentId):
        return self._topomesh.unlink(degree=dimension, wid=id, border_id=incidentId)

# Property concept

    def elementPropertyNames(self, dimension):
        return list(self._topomesh.wisp_property_names(degree=dimension))

    def hasElementProperty(self, dimension, propertyName):
        return self._topomesh.has_wisp_property(degree=dimension, property_name=propertyName)

    def elementProperty(self, dimension, propertyName):
        if propertyName in self._topomesh.wisp_property_names(dimension):
            property_dict = self._topomesh.wisp_property(degree=dimension, property_name=propertyName).to_dict()
        else:
            property_dict = dict(zip(self._topomesh.wisps(dimension), self._topomesh.wisps(dimension)))
        return property_dict
        # return dict(zip([long(k) for k in property_dict.keys()], [property_dict[k] for k in property_dict.keys()]))
        # return dict(zip(property_dict.keys(), [QVariant(property_dict[k]) for k in property_dict.keys()]))

    def addElementProperty(self, dimension, propertyName):
        return self._topomesh.add_wisp_property(degree=dimension, property_name=propertyName)

    def updateElementProperty(self, dimension, propertyName, values, eraseProperty=True):
        return self._topomesh.update_wisp_property(degree=dimension, property_name=propertyName, values=dict(zip(values.keys(), [values[k] for k in values.keys()])), erase_property=eraseProperty)

    def removeElementProperty(self, dimension, propertyName):
        return self._topomesh.remove_wisp_property(degree=dimension, property_name=propertyName)

# Orientation concept

    def orientedFaceVertexIds(self, faceId):
        if not self._topomesh.has_wisp_property('oriented_vertices', 2):
            compute_topomesh_property(self._topomesh, 'oriented_vertices', 2)
        return list(self._topomesh.wisp_property('oriented_vertices', 2)[faceId])
        # return [long(pid) for pid in ordered_pids(mesh=self._topomesh, fid=faceId)]
