from copy import deepcopy

import gnomon.core

from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import cellComplexInput, meshOutput

from cellcomplex.property_topomesh import PropertyTopomesh

from cellcomplex.property_topomesh.analysis import compute_topomesh_property, is_triangular
from cellcomplex.property_topomesh.extraction import delaunay_interface_topomesh, clean_topomesh


@algorithmPlugin(version="1.0.0", coreversion="1.0.0")
@cellComplexInput('topomesh', data_plugin="gnomonCellComplexDataPropertyTopomesh")
@meshOutput('mesh', data_plugin="gnomonMeshDataPropertyTopomesh")
class cellComplexToMeshDelaunay(gnomon.core.gnomonAbstractCellComplexAdapter):
    """Delaunay triangulation of the face vertices of a cell complex.

    """

    def __init__(self):
        super().__init__()

        self.topomesh = {}
        self.mesh = {}

    def target(self):
        return "gnomonMesh"

    def run(self):
        self.mesh = {}

        for time in self.topomesh.keys():
            topomesh = self.topomesh[time]

            if not is_triangular(topomesh):
                triangle_topomesh = delaunay_interface_topomesh(topomesh)
            else:
                triangle_topomesh = deepcopy(topomesh)

            if topomesh.nb_wisps(3) > 0:
                triangle_cells = {t:int(list(triangle_topomesh.regions(2, t))[0]) for t in triangle_topomesh.wisps(2)}
            else:
                triangle_cells = {t:int(t) for t in triangle_topomesh.wisps(2)}
            triangle_topomesh.update_wisp_property('cell', 2, triangle_cells)

            triangle_topomesh = clean_topomesh(triangle_topomesh, clean_properties=True)

            for degree in [1, 2, 3]:
                compute_topomesh_property(triangle_topomesh, 'barycenter', degree)

            for degree in [0, 1, 2, 3]:
                positions = triangle_topomesh.wisp_property('barycenter', degree)
                for k, dim in enumerate(['x', 'y', 'z']):
                    triangle_topomesh.update_wisp_property('barycenter_'+dim, degree,
                                                      dict(zip(positions.keys(), positions.values()[:, k])))

            self.mesh[time] = triangle_topomesh
