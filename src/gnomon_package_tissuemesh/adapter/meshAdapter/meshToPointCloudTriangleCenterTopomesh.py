import pandas as pd

import gnomon.core

from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import meshInput, pointCloudOutput

from cellcomplex.property_topomesh.analysis import compute_topomesh_property


@algorithmPlugin(version="1.0.0", coreversion="1.0.0")
@meshInput('mesh', data_plugin="gnomonMeshDataPropertyTopomesh")
@pointCloudOutput('df', data_plugin="gnomonPointCloudDataPandas")
class meshToPointCloudTriangleCenterTopomesh(gnomon.core.gnomonAbstractMeshAdapter):
    """Transform a mesh into the point cloud of the centers of its triangles.

    """

    def __init__(self):
        super().__init__()

        self.mesh = {}
        self.df = {}

    def target(self):
        return "gnomonPointCloud"

    def run(self):
        self.df = {}

        for time in self.mesh.keys():

            topomesh = self.mesh[time]

            compute_topomesh_property(topomesh, 'barycenter', 2)
            triangle_centers = topomesh.wisp_property('barycenter', 2)

            df = pd.DataFrame()
            df['label'] = [f for f in topomesh.wisps(2)]
            for i, dim in enumerate('xyz'):
                df['center_'+dim] = triangle_centers.values(df['label'].values)[:, i]
            self.df[time] = df
