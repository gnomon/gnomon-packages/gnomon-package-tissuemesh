import pandas as pd

import gnomon.core

from gnomon.utils import algorithmPlugin
from gnomon.utils.decorators import meshInput, pointCloudOutput


@algorithmPlugin(version="1.0.0", coreversion="1.0.0")
@meshInput('mesh', data_plugin="gnomonMeshDataPropertyTopomesh")
@pointCloudOutput('df', data_plugin="gnomonPointCloudDataPandas")
class meshToPointCloudVertexTopomesh(gnomon.core.gnomonAbstractMeshAdapter):
    """Translate a triangle mesh into a point cloud by keeping only its vertices.
    """

    def __init__(self):
        super().__init__()

        self.mesh = {}
        self.df = {}

    def target(self):
        return "gnomonPointCloud"

    def run(self):
        self.df = {}

        for time in self.mesh.keys():

            topomesh = self.mesh[time]
            vertex_positions = topomesh.wisp_property('barycenter', 0)

            df = pd.DataFrame()
            df['label'] = [v for v in topomesh.wisps(0)]
            for i, dim in enumerate('xyz'):
                df['center_'+dim] = vertex_positions.values(df['label'].values)[:, i]
            self.df[time] = df
