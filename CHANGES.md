# ChangeLog

## version 1.0.0 - 2024-03-27
* gnomon 1.0.0
* enhanced mesh visu plugin with pyvista
* new operations on meshes
* progress bar + message

## version 0.3.0 - 2023-07-10
* gnomon 0.81.0
* update to new mesh API
* changes in view behaviour

## version 0.2.0 - 2022-09-15
* gnomon 0.71.0
* enhance mesh visu plugins
