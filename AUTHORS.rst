=======
Credits
=======

Development Lead
----------------

.. {# pkglts, doc.authors

* moi, <moi@email.com>

.. #}

Contributors
------------

.. {# pkglts, doc.contributors

* Florian <florian.gacon@inria.fr>
* GACON Florian <florian.gacon@inria.fr>

.. #}
